package com.example.objecttester;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import com.google.gson.annotations.Expose;

import android.graphics.Canvas;
import android.graphics.PointF;
import android.util.Log;


public class Frame {
	
	@Expose public ArrayList<KnowledgeObject> objects;
	@Expose public ArrayList<Link> links;
	ArrayList<Frame> childFrames;
	ArrayList<Frame> parentFrames;
	private Frame lastParent;
	private FrameListener listener;
	private FrameHolder holder;
	private KnowledgeObject parentKo;
	TextBox textBox;
	TentativeLink tLink;
	Parser parser = new Parser(this);
	
	public Frame(Frame parent, FrameHolder fh, KnowledgeObject KO)
	{
		if(parent != null)
			addParent(parent);
		holder = fh;
		listener = new FrameListener(this);
		parentKo = KO;
		objects = new ArrayList<KnowledgeObject>(10);
		links = new ArrayList<Link>();
	}
	public Frame (Frame toCopy)
	{
		holder = toCopy.getHolder();
		listener = new FrameListener(this);
		objects = new ArrayList<KnowledgeObject>();
		for(KnowledgeObject ko : toCopy.objects)
		{
			HashMap<String, Object> newProperties = new HashMap<String, Object>(ko.getProperties());
			KnowledgeObject nKo = new KnowledgeObject(this, newProperties);
			objects.add(nKo);
		}
		links = new ArrayList<Link>();
		for(Link l : toCopy.links)
		{
			Link nl = new Link(this, l.getProperties());
			links.add(nl);
		}			
	}
	public Frame (Frame parent, Frame toCopy)
	{
		addParent(parent);		
		holder = toCopy.getHolder();
		listener = new FrameListener(this);
		objects = new ArrayList<KnowledgeObject>();
		for(KnowledgeObject ko : toCopy.objects)
		{
			HashMap<String, Object> newProperties = new HashMap<String, Object>(ko.getProperties());
			KnowledgeObject nKo = new KnowledgeObject(this, newProperties);
			nKo.internalFrame = new Frame(this, ko.internalFrame);
			objects.add(nKo);
		}
		links = new ArrayList<Link>();
		for(Link l : toCopy.links)
		{
			Link nl = new Link(this, l.getProperties());
			links.add(nl);
		}			
	}
	/*
	 * holder, lastParent and listener are set privately, so we only need to see them.
	 */
	public FrameHolder getHolder()
	{
		return holder;
	}
	public FrameListener getListener()
	{
		return listener;
	}
	/*
	 * Returns the last parent that accessed this frame.
	 * If frame hasn't been accessed, returns the first parent in the list.
	 * If frame has no parents, returns itself.
	 */
	public Frame getLastParent()
	{
		if(lastParent == null)
		{
			if(parentFrames == null || parentFrames.size() == 0)
			{
				Log.d("Frame", "No parents, swapping to self");
				return this;
			}
			return parentFrames.get(0);
		}
		return lastParent;
	}
	public KnowledgeObject getParentKO()
	{
		return parentKo;
	}
	/*
	 * addChild adds a child frame, addParent adds a parent frame.
	 * Both initialize their respective arraylists if they have not been initialized prior
	 */
	public void addChild(Frame toAdd)
	{
		if(childFrames == null)
			childFrames = new ArrayList<Frame>();
			
		childFrames.add(toAdd);
	}
	public void sortByZ()
	{
		Collections.sort(objects, new Comparator<KnowledgeObject>() {
			@Override
			public int compare(KnowledgeObject ko1, KnowledgeObject ko2)
			{
				int z1 = Integer.MAX_VALUE, z2 = Integer.MAX_VALUE;
				if(ko1.hasItem("zindex"))
				{
					z1 = (int)(float)(Float)ko1.get("zindex");
				}
				if(ko2.hasItem("zindex"))
				{
					z2 = (int)(float)(Float)ko2.get("zindex");
				}
				return z1-z2;				
			}
		});
	}
	public void addObjects(Collection<KnowledgeObject> kos)
	{
		if(objects == null)
			objects = new ArrayList<KnowledgeObject>();
		if(kos != null)
		{
			for(KnowledgeObject ko : kos)
			{
				objects.add(ko);
				addChild(ko.internalFrame);
			}
			sortByZ();
		}
	}
	public void addLinks(Collection<Link> newLinks)
	{
		if(links == null)
			links = new ArrayList<Link>();
		if(newLinks != null)
		{
			for(Link l : newLinks)
			{
				links.add(l);				
			}
		}
	}
	public void addParent(Frame toAdd)
	{
		if(parentFrames == null)
			parentFrames = new ArrayList<Frame>();
			
		parentFrames.add(toAdd);
	}
	/*
	 * Ensures the frame is ready to be switched to and from.
	 * Ensures frame can be entered and left without odd behaviour.
	 */
	public void accessFrame(Frame accessedFrom)
	{
		//If we're not the base frame, we have parents
		if(parentFrames != null)
		{
			//If the frame is in our parents list, we were accessed from the top.
			if(parentFrames.contains(accessedFrom))		
				{
					lastParent = accessedFrom;
				}
			//Otherwise, we're trying to go back, so don't set lastParent or we'll get stuck.
			else
			{
				Log.v("Frame", "Went backwards");
			}
		}
		//If we're here, we're the base frame, so handle that.
		else
		{
			//Handle being the base frame. It's a tough job.
		}
	}
	public ArrayList<KnowledgeObject> getSelected()
	{
		ArrayList<KnowledgeObject> tr = new ArrayList<KnowledgeObject>();
		for(KnowledgeObject ko : objects)
		{
			if(ko.isSelected())
			{
				tr.add(ko);
			}
		}
		for(Link l : links)
		{
			if(l.isSelected())
				tr.add((KnowledgeObject)l);
		}
		return tr;
	}
	public void renderAll(Canvas c)
	{
		for(KnowledgeObject KO : objects)
			KO.render(c);
		for(Link l : links)
			l.render(c);
		if(textBox!=null)
			textBox.renderMe(c);		
		if(tLink!=null)
			tLink.renderMe(c);
	}
	public String getId()
	{
		if(parentKo != null)
			return (String) parentKo.get("id");
		else
			return "root";
	}
	public void spawnTextInput(char c) {
		// TODO Auto-generated method stub
		Log.d("Frame", "Opened text input");
		ArrayList<KnowledgeObject> sel = getSelected();
		if(sel.size() >= 1)
		{
			KnowledgeObject main = sel.get(0);
			textBox = new TextBox(main.getXPos(), main.getYPos()+main.getHeight()+50);
			String s = "";
			s+=c;
			textBox.setText(s);
			sel.get(0).spawnTextInput(textBox);
		}
		else
		{
			PointF pos = listener.getLastTouch();
			textBox = new TextBox(pos.x, pos.y);
			textBox.setText(""+c);
		}
		
	}
    public void removeLinks(KnowledgeObject linked)
    {
        ArrayList<Link> linksToRemove = new ArrayList<Link>();
        for(Link l : links)
        {
            String id = (String)linked.get("id");
            if(l.getLinkedFrom().equals(id) || l.getLinkedTo().equals(id))
            {
                linksToRemove.add(l);
                //l = null;
            }
        }
        synchronized(getHolder().getThread().mRunLock)
        {
            links.removeAll(linksToRemove);
        }
    }
	public void removeTextInput() {
		// TODO Auto-generated method stub
		textBox = null;	
		ArrayList<KnowledgeObject> sel = getSelected();
		if(sel.size() == 1)
		{					
			sel.get(0).destroyTextBox();
		}		
	}
	public boolean parseInput()
	{
		Log.d("Parser", "Frame attempting to parse");
		String input = textBox.getText().trim();
		Log.d("Parser", "Parsing:" + input);
		boolean parsed = parser.executeCommand(input);
		Log.d("Parser", "Parsed as command: " + parsed);
		if(!parsed)
			{
				Log.d("Parser", "Not a command");
				parsed = parser.executeProperty(input);	
				
			}
		Log.d("Parser", "Parsed as property: " + parsed);
		if(!parsed)
			{
			Log.d("Parser", "Not a property");
				for(KnowledgeObject ko: getSelected())			
				{
					Log.d("Parser", "Setting text as " + input);
					ko.setText(input);
				}
			}
		
		removeTextInput();
		return true;
	}
	public boolean parseInput(String input)
	{
		Log.d("Parser", "Frame attempting to parse");		
		Log.d("Parser", "Parsing:" + input);
		input = input.trim();
		boolean parsed = parser.executeCommand(input);
		Log.d("Parser", "Parsed as command: " + parsed);
		if(!parsed)
			{
				Log.d("Parser", "Not a command");
				parsed = parser.executeProperty(input);	
				
			}
		Log.d("Parser", "Parsed as property: " + parsed);
		if(!parsed)
			{
			Log.d("Parser", "Not a property");
				for(KnowledgeObject ko: getSelected())			
				{
					Log.d("Parser", "Setting text as " + input);
					ko.setText(input);
				}
			}		
		return true;
	}
	public void addObject(KnowledgeObject ko)
	{
		objects.add(ko);
		sortByZ();
		for(KnowledgeObject k : objects)
		{
			if(k.hasItem("zindex"))
				Log.d("Frame", "zindex = " + k.get("zindex"));
			else
				Log.d("Frame", "zindex = undefined");
		}
		holder.space.fm.saveFile(holder.base, holder.getContext());
	}
	public void addLink(Link link)
	{
		links.add(link);
		holder.space.fm.saveFile(holder.base, holder.getContext());
	}
	/*@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		Log.d("FrameSpace", "Key Received");
		return listener.onKey(v, keyCode, event);
	}*/	
}
