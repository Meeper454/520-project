package com.example.objecttester;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.content.*;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;


public class FileManager 
{
	public void saveExternal(Frame startFrame, String filename)
	{
		Log.d("JSON", "Saving");
		if(startFrame == null)
			Log.wtf("JSON","Frame to save is null!");
		String saveString = getJSON(startFrame);
		Log.d("JSON",  saveString);
		try
		{
			FileOutputStream fos = new FileOutputStream(filename);
			fos.write(saveString.getBytes());
			fos.close();
		}
		catch(Exception ex)
		{
			Log.d("JSON", "Error while saving");
			ex.printStackTrace();
		}
	}
	public void saveAsLocal(Frame startFrame, Context c, String filename)
	{
		Log.d("JSON", "Saving");
		if(startFrame == null)
			Log.wtf("JSON","Frame to save is null!");
		String saveString = getJSON(startFrame);
		Log.d("JSON",  saveString);
		FileOutputStream fos;
		try
		{			
			fos = c.openFileOutput(filename, Context.MODE_PRIVATE);
			fos.write(saveString.getBytes());
			fos.close();
		}
		catch(Exception ex)
		{
			Log.d("JSON", "Error while saving: " + ex.getMessage());
		}
	}
	public void saveFile(Frame startFrame, Context c)
	{
		saveAsLocal(startFrame, c, "savefile.json");		
	}
	public Frame loadExternal(Frame startFrame, String filename)
	{
		String json = "";	
		try
		{			
			FileInputStream fr = new FileInputStream(filename);
			int c;
			while((c=fr.read())!= -1)
			{
				json += (char)c;
				//Log.d("JSON", json);
			}
			fr.close();
		}
		catch(Exception ex)
		{
			Log.d("JSON", "Loadfile Error: " + ex.getMessage());
		}
		return parseJSON(startFrame.getHolder(), json);
	}
	private Frame parseJSON(FrameHolder fh, String json)
	{
		Log.d("JSON", "JSON:" + json);
		//Frame parsed = new Gson().fromJson(json, Frame.class);
		//return parsed;
		if(json.equals(""))
			{						
					return new Frame(null, fh, null);				
			}
		JsonElement je = new JsonParser().parse(json);
		Log.d("JSON", "JSON Parsed");
		
		JsonObject jo = je.getAsJsonObject();		
		
		if(jo == null)
			Log.wtf("JSON", "JSON Object is null?");
		
		Log.d("JSON", "Getting objects");
		JsonArray allObjects = jo.getAsJsonArray("objects");
		Log.d("JSON", "Got objects, attempting to parse");
		Frame f = new Frame(null, fh, null);		
		ArrayList<KnowledgeObject> parsedkos = parseObjects(f,allObjects);		
		f.addObjects(parsedkos);
		JsonArray topLinks = jo.getAsJsonArray("links");
		ArrayList<Link> parsedLinks = parseLinks(f, topLinks);
		f.addLinks(parsedLinks);
		Log.v("JSON", jo.toString());
		
		return f;//Frame(null, fh, null);
	}
	public Frame loadFromLocal(Context con, FrameHolder fh, String filename)
	{
		String json = "";	
		try
		{			
			FileInputStream fr = con.openFileInput("savefile.json");
			int c;
			while((c=fr.read())!= -1)
			{
				json += (char)c;
				//Log.d("JSON", json);
			}
			fr.close();
		}
		catch(Exception ex)
		{
			Log.d("JSON", "Loadfile Error: " + ex.getMessage());
		}
		return parseJSON(fh, json);
		
	}
	public Frame loadFile(Context con, FrameHolder fh)
	{	
		return loadFromLocal(con, fh, "savefile.json");		
	}	
	private ArrayList<Link> parseLinks(Frame f, JsonArray links) 
	{
		if(links == null)
			return null;
		ArrayList<Link> newLinks = new ArrayList<Link>();
		boolean stuffToParse = links.size() > 0;
		if(!stuffToParse)
			return null;
		for(int i = 0; i < links.size(); i++)
		{
			JsonObject properties = links.get(i).getAsJsonObject().getAsJsonObject("properties");
			Log.d("JSON", "Generating hashmap");
			HashMap<String,Object> lhm = getPropertiesHashMap(properties);			
			Link nl = new Link(f, lhm);
			newLinks.add(nl);
		}
		return newLinks;
	}
	private HashMap<String, Object> getPropertiesHashMap(JsonObject properties)
	{
		HashMap<String,Object> kohm = new HashMap<String,Object>();	
		for (Map.Entry<String, JsonElement> entry : properties.entrySet()) 
		{
			String key = entry.getKey();
			boolean handled = false;
			
			JsonPrimitive value;
			Log.d("JSON", "Parsing entry: " + key);
			if(entry.getValue().isJsonPrimitive())
			{
				value = entry.getValue().getAsJsonPrimitive();


				Log.d("JSON", "Attempt to set " + key + " as " + value.getAsString());

				if(value.isBoolean())						
				{						
					kohm.put(key, value.getAsBoolean());	
					Log.d("JSON", key + " set to bool: " + value);
					handled = true;
				}
				else if(value.isNumber())
				{
					Log.d("JSON", "Value is number");
					boolean parsed = false;
					try
					{
						if (!parsed) 
						{
							int n = Integer.parseInt(value.getAsString());
							kohm.put(key, n);
							parsed = true;
							handled = true;
							Log.d("JSON", key + " set to int: " + value);
						}
					}
					catch(Exception ex)
					{
						Log.d("JSON", "Parse as int failed");
						parsed = false;
					}
					try
					{
						if (!parsed && (key.equals("width") || key.equals("height")))
						{
							double n = Double.parseDouble(value.getAsString());
							kohm.put(key, n);
							parsed = true;
							handled = true;
							Log.d("JSON", key + " set to double: " + value);
						}
					}
					catch(Exception ex)
					{
						Log.d("JSON", "Parse as double failed");
						parsed = false;
					}		
					try
					{
						if (!parsed) 
						{
							float n = Float.parseFloat(value.getAsString());
							kohm.put(key, n);
							parsed = true;
							handled = true;
							Log.d("JSON", key + " set to float: " + value);
						}
					}
					catch(Exception ex)
					{
						Log.d("JSON", "Parse as float failed");
						parsed = false;
					}

				}
				else if(value.isString())
				{
					try
					{
						kohm.put(key, value.getAsString());
						handled = true;
						Log.d("JSON", key + " set to string: " + value);
					}
					catch(Exception ex)
					{
						Log.e("Error", "Value not a string");
					}
				}
			}					
			else
			{
				Log.wtf("JSON", "????");
			}
			Log.d("JSON", "Handled = " + handled);
		}
		
		String type = properties.get("type").getAsString();
		
		if(type.equals("KO_SQUARE"))
		{
			kohm.put("type", KOType.KO_SQUARE);
		}
		else if(type.equals("KO_CIRCLE"))
		{					
			kohm.put("type", KOType.KO_CIRCLE);
		}
		else if(type.equals("KO_TEXT"))
		{
			kohm.put("type", KOType.KO_TEXT);
		}
		else if(type.equals("KO_LINK"))
		{
			kohm.put("type", KOType.KO_LINK);
		}
		Log.d("JSON", "Making knowledge object");
		Log.d("JSON", "Hashmap OK:" + (kohm != null));		
		return kohm;
	}
	private ArrayList<KnowledgeObject> parseObjects(Frame parentFrame, JsonArray objects)
	{
		try
		{
			Log.d("JSON", "Checking for objects");
			boolean stuffToParse = objects.size() > 0;
			if(!stuffToParse)
				return null;

			//KnowledgeObject newKO = new KnowledgeObject();
			ArrayList<KnowledgeObject> kos = new ArrayList<KnowledgeObject>();
			for(int i = 0; i < objects.size(); i++)
			{
				JsonArray frameObjects = null;
				try
				{
					Log.d("JSON", "Getting child objects");		
					frameObjects = objects.get(i).getAsJsonObject().getAsJsonObject("internalFrame").getAsJsonArray("objects");
				}
				catch(Exception ex)
				{
					Log.wtf("JSON", "No internalFrame. Possible link being parsed as KO.");
				}
				Log.d("JSON", "Getting properties");
				JsonObject properties = objects.get(i).getAsJsonObject().getAsJsonObject("properties");
				Log.d("JSON", "Generating hashmap");
				HashMap<String,Object> kohm = getPropertiesHashMap(properties);								
				Log.d("JSON", "Populating children");
				KnowledgeObject toAdd;				
				toAdd = new KnowledgeObject(parentFrame, kohm);
				Frame internalFrame = toAdd.internalFrame;
				JsonArray topLinks = objects.get(i).getAsJsonObject().getAsJsonArray("links");
				ArrayList<Link> parsedLinks = parseLinks(internalFrame, topLinks);
				internalFrame.addLinks(parsedLinks);
				internalFrame.addObjects(parseObjects(internalFrame, frameObjects));
				Log.d("JSON", "Children populated, adding to list");
				kos.add(toAdd);

				Log.d("JSON", properties.toString());
			}
			Log.d("JSON", "List complete");
			return kos;
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}		
		
		return null;
		
	}
	private String getJSON(Frame startFrame)
	{
		if(startFrame.objects.size() == 0)
			return "";
		else
		{
			Gson g = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
			String returnString = "";		
			returnString += g.toJson(startFrame);
			Log.d("JSON", returnString);				
				//returnString += getJSON(ko.internalFrame);			
			return returnString;			
		}
	}
}
