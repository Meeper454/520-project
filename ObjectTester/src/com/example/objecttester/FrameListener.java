package com.example.objecttester;

import java.util.ArrayList;
import java.util.Collections;

import android.annotation.SuppressLint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;

public class FrameListener implements OnTouchListener, KeyEvent.Callback, OnLongClickListener{

	Frame listenTo;
	
	private boolean sameObject = false;
	private boolean processingTouch = false;
	private float touchX1, touchY1;
	private float touchX2, touchY2;
	private KnowledgeObject touchKo1 = null;
	private KnowledgeObject touchKo2 = null;
	private boolean moving = false;	
	private boolean linking = false;
	private boolean selecting = false;
	private CommandBinder binder;
	
	private ArrayList<PointF> drawnPath;	
	private float circleSize;
	
	
	public FrameListener(Frame toListen)
	{
		listenTo = toListen;
		drawnPath = new ArrayList<PointF>();
		binder = new CommandBinder(this);
	}
	
	public PointF getLastTouch()
	{
		return new PointF(touchX1, touchY1);
	}
	
	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		boolean returnVal = false;
		if(!processingTouch)
		{
			processingTouch = true;
			//Log.d("FrameSpace","Touch Detected");
			if(event.getPointerCount() == 1)
			{
				
				switch(event.getAction())		
				{
					case MotionEvent.ACTION_DOWN:
						drawnPath.clear();
						touchX1 = event.getX();
						touchY1 = event.getY();						
						
						touchKo1 = findKOByCoords(touchX1, touchY1);
						
						if(touchKo1 == null)
							touchKo1 = findLinkByCoords(touchX1, touchY1);
						
						if(touchKo1 != null)
							{	
								if(!touchKo1.isSelected() && !selecting)
								{
									deselectAll();
									touchKo1.selectMe();									
									returnVal = true;
								}
								else if(selecting)
								{
									if(touchKo1.isSelected())
										touchKo1.deSelectMe();
									else
										touchKo1.selectMe();
								}
								if(onLine(touchX1, touchY1))
								{
									Log.d("Link", "Link start");
									linking = true;
								}
							}
						Log.d("FrameSpace", "Ko = " + (touchKo1 == null));
						break;
					case MotionEvent.ACTION_MOVE:
					if(touchKo1 != null)
						{
							
							float dx = Math.abs(touchX1 - event.getX()), dy = Math.abs(touchY1 - event.getY());
							if((dx > 20 || dy > 20 || moving) && !linking)
							{
								moving = true;
								touchX1 = event.getX();
								touchY1 = event.getY();
								//Assume user is trying to move object

								int drag = touchKo1.cornerDrag(touchX1, touchY1);
								PointF p = new PointF(touchX1, touchY1);								
								switch(drag)
								{								
									case 0:
										float offX, offY;
										offX = touchX1 - touchKo1.getXPos() - touchKo1.getWidth()/2;;
										offY = touchY1 - touchKo1.getYPos() - touchKo1.getHeight()/2;;
										Log.d("FrameListener", "OffX = " + offX + "; OffY = " + offY);
										for(KnowledgeObject ko: listenTo.getSelected())
										{
                                            KnowledgeObject copyInto = null;
                                            if((copyInto = findKOByCoords(touchX1,touchY1, listenTo.getSelected())) != null)
                                            {
                                                listenTo.getHolder().clipBoard.softCut(listenTo.getSelected());
                                                listenTo.getHolder().swapFrame(copyInto.internalFrame);
                                                listenTo.getHolder().clipBoard.softPaste(copyInto.internalFrame);
                                            }
                                            float x = ko.getXPos();
                                            float y = ko.getYPos();
                                            doMove(ko, x + offX , y + offY);
										}
										//doMove(touchKo1, touchX1, touchY1);
										break;
									case 1:
										touchKo1.setScaling(true);
										touchKo1.topLeftScale(p);
										break;
									case 2:
										touchKo1.setScaling(true);
										touchKo1.topRightScale(p);
										break;
									case 3:
										touchKo1.setScaling(true);
										touchKo1.botRightScale(p);
										break;
									case 4:
										touchKo1.setScaling(true);
										touchKo1.botLeftScale(p);
										break;
								}
								returnVal = true;
							}
							else if(linking)
							{
								Log.d("Link", "Linking");
								touchX1 = event.getX();
								touchY1 = event.getY();
								if(listenTo.tLink == null)
								{
									listenTo.tLink = new TentativeLink(touchKo1, touchX1, touchY1);
								}
								else
								{
									Log.d("Link", "Setting end point");
									listenTo.tLink.setEndpoint(new PointF(touchX1, touchY1));
								}
								Log.d("Link", "Extrapolating link");
								listenTo.tLink.extrapolate();
								returnVal = true;
							}
						}						
						else if(!linking)
						{
							moving = true;
							PointF np = new PointF(event.getX(), event.getY());
							drawnPath.add(np);
							returnVal=true;
						}						
						break;
					case MotionEvent.ACTION_UP:
						
						//Unnecessary when not moving a clone
						if(touchKo1 != null)
						{
							touchKo1.setScaling(false);						
						}						
						if(drawnPath.size() > 0)
						{
							if(isCircle(drawnPath, 0.2f) && touchKo1 == null)
							{
								PointF centre = getCentre(drawnPath);								
								float radius = circleSize/2;
								RectF area = new RectF(centre.x - radius, centre.y - radius, centre.x + radius, centre.y + radius);
								ArrayList<KnowledgeObject> obj = findKOsByArea(area);								
								if(obj.size() > 0)
								{
									//Select All. Multiple selection unimplemented
									Log.d("FrameSpace", "Objects in circle");
									for(KnowledgeObject ko : obj)
									{
										ko.selectMe();
									}
								}
								else
								{
									//Had to invert x and y here. Examine earlier code
									KnowledgeObject ko = new KnowledgeObject(listenTo, new PointF(area.left, area.top), area.width(), area.height(), KOType.KO_CIRCLE);
									ko.selectMe();
									synchronized(listenTo.getHolder().getThread().mRunLock)
									{
										listenTo.addObject(ko);
									}
								}
								returnVal = true;
							}
							
							else
								{
									deselectAll();
									Log.d("FrameSpace", "Not a circle");
								}
						}
						else if (linking)
						{
							Log.d("Link", "Making link");	
							KnowledgeObject linkTo = null;
							if(listenTo != null && listenTo.tLink != null)
							linkTo  = listenTo.tLink.getLinkKo();
							if(linkTo != null)
							{
								String id = (String)linkTo.get("id");
								Log.d("Link", "Linking " + (String)touchKo1.get("id") + " to " + id);
								Link nLink = new Link(listenTo, (String)touchKo1.get("id"), id);
								synchronized(listenTo.getHolder().getThread().mRunLock)
								{
									listenTo.addLink(nLink);
								}								
							}
							else
								Log.d("Link", "No link made");
							listenTo.tLink = null;
							linking = false;
							returnVal = true;
						}
						moving = false;
						linking=false;
						listenTo.getHolder().space.fm.saveFile(listenTo.getHolder().base, listenTo.getHolder().getContext());
						break;				
				}
				processingTouch = false;			
				Log.d("FrameListener", "returnVal = " + returnVal);
				return returnVal;
			}
			else if (event.getPointerCount() == 2)
			{
				Log.d("FrameSpace", "2 Pointers detected");
				switch(event.getActionMasked())		
				{
					case MotionEvent.ACTION_DOWN:
						touchX1 = event.getX();
						touchY1 = event.getY();
						touchKo1 = findKOByCoords(touchX1, touchY1);
						break;
					case MotionEvent.ACTION_MOVE:
						break;
					case MotionEvent.ACTION_UP:
					case MotionEvent.ACTION_POINTER_UP:
						if(sameObject)
						{
							if(touchKo1 == null || touchKo2 == null)
								{
									Log.wtf("FrameSpace", "No idea what's going on here. Ignore input, I guess.");
									processingTouch = false;
									returnVal  = false;
								}
							KOType types[] = KOType.values();
							KOType obtype = touchKo1.getType();
							KOType newType = types[0];
							for(int i = 0; i < types.length; i++)
							{
								if(types[i].equals(obtype))
								{
									if(i != types.length - 2)
									{
										newType = types[i + 1];
									}
									else
										newType = types[0];
								}
							}
							synchronized(listenTo.getHolder().getThread().mRunLock)							
								{
									touchKo1.setType(newType);
								}
							//touchKo1 = null; touchKo2 = null;
						}
						listenTo.getHolder().space.fm.saveFile(listenTo.getHolder().base, listenTo.getHolder().getContext());
						break;
					case MotionEvent.ACTION_POINTER_DOWN:
						touchX2 = event.getX();
						touchY2 = event.getY();
						touchKo2 = findKOByCoords(touchX2, touchY2);
						if(touchKo1 == null || touchKo2 == null)
							{
								Log.d("FrameSpace", "Knowledgeobject(s) are null, handle this");
								break;
							}
						sameObject = (touchKo1.equals(touchKo2) && touchKo1 != null && touchKo2 != null);					
						break;
				}
				processingTouch = false;
				return true;
			}			
		}	
		//Log.d("FrameListener", "returnVal = " + returnVal);
		return returnVal;		
	}
	
	private KnowledgeObject findLinkByCoords(float x, float y) 
	{
		for(KnowledgeObject ko: listenTo.links)
		{
			RectF koRect = ko.getRect();
			
			Log.d("FrameSpace", "Coords: x = " + x + ", y = " + y);
			Log.d("FrameSpace", "RectF: topleft = " + koRect.left + "," + koRect.top + " botright = " + koRect.right + "," + koRect.bottom);
			Log.d("FrameSpace", "contains = " + koRect.contains(x,y));		
			
			if(koRect.contains(x,y))
				return ko;			
		}		
		return null;
	}

	private boolean onLine(float x, float y) 
	{
		if(touchKo1.getType() != KOType.KO_CIRCLE)		
			return false;
		else
		{
			float a = touchKo1.getXPos() + touchKo1.getWidth()/2;
			float b = touchKo1.getYPos() + touchKo1.getHeight()/2;
			
			double dx = x - a;
			double dy = y - b;
			double angle = Math.atan2(dy,dx);
			
			//angle -= Math.PI / 8;
			Log.d("Link", "Angle = " + Math.toDegrees(angle));
			
			float radius1 = touchKo1.getWidth()/2;
			float radius2 = radius1 - 35;//(int)touchKo1.get("strokewidth");
			
			
			
			double innerX = a + radius2 * Math.cos(angle);
			double outerX = a + radius1 * Math.cos(angle);	
			
			boolean withinX = (innerX < x) && (outerX > x);
			withinX |= (outerX < x) && (innerX > x);
			
			double innerY = b + radius2 * Math.sin(angle);
			double outerY = b + radius1 * Math.sin(angle);
			
			boolean withinY = (innerY < y) && (outerY > y);			
			withinY |= (outerY < y) && (innerY > y);
			
			Log.d("Link", "Inner Coords = " + innerX + ", " + innerY);
			Log.d("Link", "Touch Coords = " + x + ", " + y);
			Log.d("Link", "Outer Coords = " + outerX + ", " + outerY);		
			
			boolean online = (withinX && withinY);
			Log.d("Link", "On Line = " + online);
			return online;
		}
	}

	public KnowledgeObject findKOByCoords(float x, float y)
	{
		ArrayList<KnowledgeObject> reverseKo = listenTo.objects;
		Collections.reverse(reverseKo);
		for(KnowledgeObject ko: reverseKo)
		{
			RectF koRect = ko.getRect();
			
			Log.d("FrameSpace", "Coords: x = " + x + ", y = " + y);
			Log.d("FrameSpace", "RectF: topleft = " + koRect.left + "," + koRect.top + " botright = " + koRect.right + "," + koRect.bottom);
			Log.d("FrameSpace", "contains = " + koRect.contains(x,y));		
			
			if(koRect.contains(x,y) || ko.cornerDrag(x,y) > 0)
				return ko;			
		}
		return null;		
	}
    public KnowledgeObject findKOByCoords(float x, float y, ArrayList<KnowledgeObject> exclude)
    {
        ArrayList<KnowledgeObject> reverseKo = listenTo.objects;
        Collections.reverse(reverseKo);
        for(KnowledgeObject ko: reverseKo)
        {
            RectF koRect = ko.getRect();

            Log.d("FrameSpace", "Coords: x = " + x + ", y = " + y);
            Log.d("FrameSpace", "RectF: topleft = " + koRect.left + "," + koRect.top + " botright = " + koRect.right + "," + koRect.bottom);
            Log.d("FrameSpace", "contains = " + koRect.contains(x,y));

            if((koRect.contains(x,y) || ko.cornerDrag(x,y) > 0) && !exclude.contains(ko))
                return ko;
        }
        return null;
    }
	public ArrayList<KnowledgeObject> findKOsByArea(RectF area)
	{
		ArrayList<KnowledgeObject> toReturn = new ArrayList<KnowledgeObject>();
		for(KnowledgeObject ko : listenTo.objects)
		{
			if (area.contains(ko.getRect()))
			{
				toReturn.add(ko);
			}
		}
		return toReturn;
	}
	private float getDistance(PointF p1, PointF p2) {
	    return (float) Math.sqrt((p2.x-p1.x)*(p2.x-p1.x)+(p2.y-p1.y)*(p2.y-p1.y));
	}
	private PointF getCentre(ArrayList<PointF> points)
	{
		float xs = 0; float ys = 0;
		for(PointF p : points)
		{
			xs += p.x;
			ys += p.y;
		}
		return new PointF(xs/points.size(), ys/points.size());
	}	
	public boolean isCircle(ArrayList<PointF> points, float error) 
	{
	    if(points.size() <= 10)
	    	{
	    		Log.d("FrameSpace","Not enough Points");
	    		return false;
	    	}
	    ArrayList<Float> weights = new ArrayList<Float>();
	    float maxDistance = 0;
	    float sumDistance = 0;
	    float avgDistance = 0;
	    float errorConstraint = 0;
	    for(int i=0; i<points.size(); i++) 
	    {
	    	float distance = 0;
	        for(int j=0; j < points.size(); j++) 
	        {
	            float d = getDistance(points.get(i), points.get(j));
	            if(d > distance) 
	            {
	                distance = d;
	            }
	        }
	        if(distance > 0) 
	        {
	            if(distance > maxDistance) maxDistance = distance;
	            sumDistance += distance;
	            weights.add(distance);
	        }
	    }	    
	    avgDistance = sumDistance / weights.size();
	    circleSize = avgDistance; 
	    errorConstraint = error * avgDistance;
	    for(int i=0; i < weights.size(); i++) 
	    {
	        if(Math.abs(avgDistance - weights.get(i)) > errorConstraint) 
	        {
	            return false;
	        }
	    }
	    if(avgDistance < 5)
	    	return false;
	    return true;
	}
	public void deselectAll()
	{
		listenTo.removeTextInput();
		synchronized(listenTo.getHolder().getThread().mRunLock)
		{
			for(KnowledgeObject ko: listenTo.objects)
			{
				ko.deSelectMe();
			}			
		}
		synchronized(listenTo.getHolder().getThread().mRunLock)
		{
			for(KnowledgeObject ko: listenTo.links)
			{
				ko.deSelectMe();
			}			
		}		
	}
	public void doMove(KnowledgeObject ko, float x, float y)
	{
		ko.move(x /*- ko.getWidth()/2*/, y /*- ko.getHeight()/2*/);
	}

	
	@Override
	public boolean onLongClick(View arg0) 
	{
		// TODO Auto-generated method stub
		Log.d("FrameSpace", "Long click detected");
		KnowledgeObject ko = findKOByCoords(touchX1, touchY1);
		if(ko != null && !moving && !linking)
		{
			Log.d("FrameSpace", "Attempting frame swap");
			if(ko.internalFrame != null)
				listenTo.getHolder().swapFrame(ko.internalFrame);
			return true;
		}
		return false;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		Log.d("FrameListener", "Keydown detected");
		switch(keyCode)
		{
		case KeyEvent.KEYCODE_SHIFT_LEFT:
		case KeyEvent.KEYCODE_SHIFT_RIGHT:
			selecting = true;
			Log.d("FrameListener", "Shift held down");
			break;	
		case KeyEvent.KEYCODE_S:
			if(event.hasModifiers(KeyEvent.META_SHIFT_ON | KeyEvent.META_CTRL_ON))
			{
				listenTo.getHolder().space.saveExternalFile();				
			}
			break;
		case KeyEvent.KEYCODE_L:
			if(event.hasModifiers(KeyEvent.META_SHIFT_ON | KeyEvent.META_CTRL_ON))
			{
				listenTo.getHolder().space.loadExternalFile();				
			}
		}		
		return true;
	}

	@Override
	public boolean onKeyLongPress(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onKeyMultiple(int keyCode, int count, KeyEvent event) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) 
	{
		// TODO Auto-generated method stub
		Log.d("FrameSpace", "KeyUp detected");
	if(keyCode == KeyEvent.KEYCODE_SHIFT_LEFT || keyCode == KeyEvent.KEYCODE_SHIFT_RIGHT)
			selecting = false;			
		
	
		if(keyCode == KeyEvent.KEYCODE_FORWARD_DEL)
		{
			Log.d("FrameSpace", "Delete detected");
			ArrayList<KnowledgeObject> kos = listenTo.getSelected();					
			for(KnowledgeObject ko : kos)
			{
					ko.destroy();				
			}
			synchronized(listenTo.getHolder().getThread().mRunLock)
			{				
				listenTo.objects.removeAll(kos);
				listenTo.links.removeAll(kos);
			}
			listenTo.getHolder().space.fm.saveFile(listenTo.getHolder().base, listenTo.getHolder().getContext());
			return true;
		}
		else if(keyCode == KeyEvent.KEYCODE_BACK)
		{
			listenTo.getHolder().swapFrame(listenTo.getLastParent());
			return true;
		}
		else if(keyCode == KeyEvent.KEYCODE_A && event.isCtrlPressed() && event.isShiftPressed())				
		{
			Log.d("FrameListener", "A pressed");
			if(listenTo.textBox == null)
			{				
					Log.d("FrameListener", "Selecting all");
					for(KnowledgeObject ko: listenTo.objects)
					{
						ko.selectMe();
					}
					return true;				
			}
		}
		else if(keyCode == KeyEvent.KEYCODE_C && event.isCtrlPressed() && event.isShiftPressed())
		{
			listenTo.getHolder().clipBoard.copy(listenTo.getSelected());
		}
		else if(keyCode == KeyEvent.KEYCODE_X && event.isCtrlPressed()&& event.isShiftPressed())
		{
            listenTo.getHolder().clipBoard.cut(listenTo.getSelected());
		}
		else if(keyCode == KeyEvent.KEYCODE_V && event.isCtrlPressed()&& event.isShiftPressed())
		{
			listenTo.getHolder().clipBoard.paste(listenTo);
		}
		else if(event.isCtrlPressed())
		{
			binder.findCommandKey(keyCode, event);
		}
		else if(keyCode == KeyEvent.KEYCODE_CTRL_LEFT || keyCode == KeyEvent.KEYCODE_CTRL_RIGHT)
		{
			binder.executeCommandKey(event);
		}
		else
		{
			if(listenTo.textBox == null && event.isPrintingKey())
				listenTo.spawnTextInput((char)event.getUnicodeChar());			
			else if(listenTo.textBox != null)
			{
				Log.d("FrameListener", "Keycode: " + keyCode);
				switch(keyCode)
				{ 
					case KeyEvent.KEYCODE_DEL:
						listenTo.textBox.removeLastChar();
						break;
					case KeyEvent.KEYCODE_ENTER:
						Log.d("FrameListener", "Enter pressed");
						listenTo.parseInput();
						listenTo.getHolder().space.fm.saveFile(listenTo.getHolder().base, listenTo.getHolder().getContext());
						break;					
					default:
						if(!event.isPrintingKey() && keyCode != KeyEvent.KEYCODE_SPACE)
							break;
						int ucode = event.getUnicodeChar(event.getMetaState());
						if(ucode > 31)
						{
							Log.d("Parser", "Character = " + ucode);						
							listenTo.textBox.appendText((char)ucode);
						}
						break;				
				}
				
			}
		}
		return false;
	}
	
	
}
