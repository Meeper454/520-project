package com.example.objecttester;

import android.graphics.Canvas;

import com.example.objecttester.render.RTextBox;

public class TextBox 
{
	private String inputText;
	private float x,y;
	private RTextBox render = new RTextBox(this);	
	
	public TextBox(float x, float y)
	{
		setX(x);
		setY(y);
	}	
	public void renderMe(Canvas c)
	{
		render.renderMe(c);
	}
	public float getX() 
	{
		return x;
	}

	public void setX(float x) 
	{
		this.x = x;
	}

	public float getY() 
	{
		return y;
	}

	public void setY(float y) 
	{
		this.y = y;
	}

	public String getText()
	{
		return inputText;
	}
	
	public void setText(String in)
	{
		inputText = in;
	}
	
	public void appendText(String ap)
	{
		inputText += ap;
	}
	public void appendText(char ap)
	{
		inputText += ap;
	}
	public void removeLastChar()
	{
		if(!inputText.equals(""))
		{
			inputText = inputText.substring(0, inputText.length() - 1);
		}
	}
}
