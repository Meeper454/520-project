package com.example.objecttester;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

public class Clipboard 
{
	private ArrayList<KnowledgeObject> items;
	private FrameHolder holder;
	private ArrayList<KnowledgeObject> softItems;
	
	public Clipboard(FrameHolder _holder)
	{
		items = new ArrayList<KnowledgeObject>();
		softItems = new ArrayList<KnowledgeObject>();
		holder =  _holder;
	}	
	public void copy(ArrayList<KnowledgeObject> toCopy)
	{
		for(KnowledgeObject ko : toCopy)
		{
			HashMap<String, Object> newProperties = new HashMap<String, Object>(ko.getProperties());			
			KnowledgeObject nKo = new KnowledgeObject(null, newProperties);
			nKo.internalFrame = new Frame(ko.internalFrame);
			items.add(nKo);
            String id  = (String)ko.get("id");
            nKo.set("id", id + ".");
            Log.d("Clipboard", "Copied : " + id);
		}		
	}	
	public void cut(ArrayList<KnowledgeObject> toCut)
	{
		items = toCut;
        for(KnowledgeObject ko : toCut)
        {
            ko.parentFrame.removeLinks(ko);
        }
        synchronized (holder.getThread().mRunLock)
        {
            holder.renderFrame.objects.removeAll(toCut);
        }
	}
	public void softCopy(ArrayList<KnowledgeObject> toCopy)
	{
        for(KnowledgeObject ko : toCopy)
        {
            HashMap<String, Object> newProperties = new HashMap<String, Object>(ko.getProperties());
            KnowledgeObject nKo = new KnowledgeObject(null, newProperties);
            nKo.internalFrame = new Frame(ko.internalFrame);
            items.add(nKo);
            String id  = (String)ko.get("id");
            Log.d("Clipboard", "Copied : " + id);
        }
    }
    public void softCut(ArrayList<KnowledgeObject> toCut)
    {
        items = toCut;
        for(KnowledgeObject ko : toCut)
        {
            ko.parentFrame.removeLinks(ko);
        }
        synchronized (holder.getThread().mRunLock)
        {
            holder.renderFrame.objects.removeAll(toCut);
        }
    }
    public void clearClipboard(KnowledgeObject toClear)
    {
        items.remove(toClear);
    }
	public void paste(Frame pasteTo)
	{
		for(KnowledgeObject ko : items)
		{
            if(ko != null)
            {
                ko.internalFrame.addParent(pasteTo);
                ko.parentFrame = pasteTo;
                String id = (String) ko.get("id");
                Log.d("Clipboard", "pasted : " + id);
            }
		}
        synchronized(holder.getThread().mRunLock)
        {
            pasteTo.objects.addAll(items);
        }
	}
	public void softPaste(Frame pasteTo)
	{
		for(KnowledgeObject ko : softItems)
		{
			ko.internalFrame.addParent(pasteTo);
            ko.parentFrame = pasteTo;
		}
        synchronized(holder.getThread().mRunLock)
        {
            pasteTo.objects.addAll(items);
        }
		softItems.clear();
	}	
}
