package com.example.objecttester;

import java.util.ArrayList;

import android.util.Log;
import android.view.KeyEvent;

public class CommandBinder 
{
	private FrameListener listener;
	private ArrayList<KnowledgeObject> commandObjects = new ArrayList<KnowledgeObject>();
	private KnowledgeObject cmd_exe = null;
	private KnowledgeObject cpos = null;
	private String s = "";
	public CommandBinder(FrameListener l)
	{
		listener = l;
	}	
	private void findAllCommands()
	{
		Frame base = listener.listenTo.getHolder().base;
		KnowledgeObject baseKo = null;
		for(KnowledgeObject ko : base.objects)
		{
			String id = (String) ko.get("id");
			if(id.equals("ctrl_cmds"))
			{
				baseKo = ko;				
			}
		}
		if(baseKo != null)
			commandObjects.addAll(baseKo.internalFrame.objects);
	}
	private boolean hasCmds(KnowledgeObject check)
	{
		boolean hasCmds = false;
		if(check.internalFrame.objects.size() == 0)			
			return hasCmds;
		else
		{
			for(KnowledgeObject ko : check.internalFrame.objects)
			{
				String id = (String)ko.get("id");
				if(id.length() == 1)
				{
					hasCmds = true;
				}
			}
		}
		return hasCmds;
	}
	public void findCommandKey(int keyCode, KeyEvent event)
	{
		findAllCommands();
		cmd_exe = null;
		
		int c = event.getUnicodeChar(0);
		if(event.isShiftPressed())
		{
			c = Character.toUpperCase(c);
		}
		ArrayList<KnowledgeObject> scan = commandObjects;
		if(cpos != null)
			scan = cpos.internalFrame.objects;
		Log.d("Parser", "Char = " + (char)c);
		String key = "" + (char)c;
		boolean found = false;
		if(c == 0)
		{
			return;
		}
		for(KnowledgeObject ko : scan)
		{
			if(ko.get("id").equals(key))
			{
				cmd_exe = ko;
				found = true;
			}
		}
		if(found)
		{
			if(cmd_exe.internalFrame.objects.size() == 0)
			{
				//executeCommandKey(event);				
			}
			else if (!hasCmds(cmd_exe))
			{
				executeCommandKey(event);
			}
			else
			{
				s += (char)c;
				cpos = cmd_exe;
			}
		}
		else
		{
			cpos = null;
		}
	}
	public void executeCommandKey(KeyEvent event)
	{
		Log.d("Binder", "Command sequence: " + s);
		if(cmd_exe != null)
		{
			for(KnowledgeObject ko: cmd_exe.internalFrame.objects)		
			{
				String id = (String) ko.get("id");
				if(id.length() > 1)
				{
					String cmd = ko.getText();
					if(!cmd.startsWith("@"))
						cmd = "@"+cmd;
					listener.listenTo.parseInput(cmd);
				}
			}
		}
		cpos = null;
		s="";
	}
}
