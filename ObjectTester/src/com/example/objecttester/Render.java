package com.example.objecttester;

import com.example.objecttester.render.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.graphics.Canvas;
//import android.graphics.Point;
import android.util.Log;

public class Render {
	
	private ArrayList<Render> renderers;
	//private Canvas renderCanvas;	
	protected KnowledgeObject objectToRender;
	
	protected renderType rType;	
	public Render(KnowledgeObject ko)
	{
		//renderCanvas = c;
		objectToRender = ko;
		renderers = new ArrayList<Render>(5);
		rType = renderType.BASE;
		addRenderer("Fill");
		addRenderer("Outline");
		KOType type = ko.getType();		
		//addRenderer(type.toString());		
		if(type.ordinal() < KOType.KO_TEXT.ordinal())
			addRenderer("text");		
	}
	public Render() {
		// TODO Auto-generated constructor stub
		//return;
	}
	public void destroy()
	{
		for(int i = 0; i < renderers.size(); i++)
		{
			
			renderers.get(i).objectToRender = null;			
		}
		renderers.clear();
	}
	public Render clone(KnowledgeObject assoc)
	{
		Render toReturn = new Render(assoc);
		toReturn.renderers = new ArrayList<Render>(renderers.size());
		for(int i = 0; i < renderers.size(); i++)
		{
			if(!renderers.get(i).getType().equals(renderType.RT_SELECT))
			{
				try 
				{
					toReturn.renderers.add((Render) renderers.get(i).clone());
				}
				catch (CloneNotSupportedException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return toReturn;
	}
	private void sort()
	{
		/* Render order
		 * 0: Shadow 
		 * 1-4: Object
		 * 5: InnerText
		 * 6: Selection Box
		 */	
		if(renderers.size() < 2)
			return;
				
		for(int i = 0; i < renderers.size(); i++)			
		{				
			Log.d("FrameSpace", (i + 1) + "th Renderer = " + renderers.get(i).getType());
		}
		
		Collections.sort(renderers, new Comparator<Render>() {
			@Override
			public int compare(Render r1, Render r2)
			{
				return r1.getType().compareTo(r2.getType());
			}
		});				
		
		
	}
	public renderType getType()
	{
		return rType;
	}
	private void doAddRenderer(String renderName)
	{
		if(renderName.equals("Outline"))
		{
			Outline out = new Outline(objectToRender);
			renderers.add(out);
		}
		if(renderName.equals("Fill"))
		{
			Fill fill = new Fill(objectToRender);
			renderers.add(fill);
		}		
		else if (renderName.equals("text"))
		{
			InnerText t = new InnerText(objectToRender);
			renderers.add(t);
		}
		else if (renderName.equals("select"))
		{
			Selected s = new Selected(objectToRender);
			renderers.add(s);
		}
		else if (renderName.equals("shadow"))
		{
			DropShadow ds = new DropShadow(objectToRender);
			renderers.add(ds);
		}	
		else if (renderName.equals("moving"))
		{
			Moving m = new Moving(objectToRender);
			renderers.add(m);
		}
		else if(renderName.equals("textbox"))
		{
			RTextBox tb = new RTextBox(objectToRender.tb);
			renderers.add(tb);
		}		
		sort();
	}	
	public void addRenderer(String renderName)
	{
		if(objectToRender.parentFrame != null)
		{
			synchronized(objectToRender.parentFrame.getHolder().getThread().mRunLock)
			{
				doAddRenderer(renderName);
			}
		}
		else 
			doAddRenderer(renderName);
	}
	private void doRemoveRenderer(String renderName)
	{
		if(renderName.contains("KO"))
		{
			removeKORenderer();
		}
		else if(renderName.equals("shadow"))
			removeDropShadow();
		else if(renderName.equals("select"))
			removeSelect();
		else if (renderName.equals("text"))
			removeInnerText();
		else if (renderName.equals("textbox"))
			removeTextBox();
	}
	public void removeRenderer(String renderName)
	{
		if (objectToRender.parentFrame != null) 
		{
			synchronized (objectToRender.parentFrame.getHolder().getThread().mRunLock) 
			{
				doRemoveRenderer(renderName);
			}
		}
		else
			doRemoveRenderer(renderName);
	}
	private void removeTextBox() 
	{		
		for(int i = 0; i < renderers.size(); i++)
		{
			Render r = renderers.get(i);
			renderType rt = r.getType();
			if(rt.equals(renderType.RT_TEXTBOX))
			{				
				renderers.remove(i);
			}		
		}
	}
	private void removeKORenderer()
	{
		for(int i = 0; i < renderers.size(); i++)
		{
			Render r = renderers.get(i);
			renderType rt = r.getType();
			if(rt.equals(renderType.RT_FILL) || rt.equals(renderType.RT_OUTLINE))
			{					
				renderers.remove(i);
			}		
		}
		
	}
	private void removeDropShadow()
	{
		for(int i = 0; i < renderers.size(); i++)
		{
			Render r = renderers.get(i);
			renderType rt = r.getType();
			if(rt.equals(renderType.RT_SHADOW))
			{					
				renderers.remove(i);
			}		
		}
	}
	private void removeSelect()
	{
		for(int i = 0; i < renderers.size(); i++)
		{
			Render r = renderers.get(i);
			renderType rt = r.getType();
			if(rt.equals(renderType.RT_SELECT))
			{					
				renderers.remove(i);
			}		
		}
	}
	private void removeInnerText()
	{
		for(int i = 0; i < renderers.size(); i++)
		{
			Render r = renderers.get(i);
			renderType rt = r.getType();
			if(rt.equals(renderType.RT_INNERTEXT))
			{					
				renderers.remove(i);
			}		
		}
	}
	public void renderAll(Canvas renderCanvas)
	{
		for(Render r : renderers)
		{
			r.renderMe(renderCanvas);
		}
	}
	public void toggleShadow()
	{
		addRenderer("shadow");
	}
	public void renderMe(Canvas c) {
	}
	public void expandMe(Canvas c)
	{	
		//expandRect(c)
	}
	@SuppressWarnings("unused")
	private void expandCircle(Canvas c)
	{
		KnowledgeObject tmp = objectToRender;
		
		for(float f = tmp.getWidth(); f < c.getWidth(); f+=1)
		{
			tmp.setSize(f, tmp.getHeight() + 1 );
			if(tmp.getXPos() + tmp.getWidth()/2 > c.getWidth()/2)
			{
				tmp.setPos(tmp.getXPos() - 1, tmp.getYPos());
			}
			if(tmp.getXPos() + tmp.getWidth()/2 < c.getWidth()/2)
			{
				tmp.setPos(tmp.getXPos() + 1, tmp.getYPos());
			}
			if(tmp.getYPos() + tmp.getHeight()/2 > c.getHeight()/2)
			{
				tmp.setPos(tmp.getXPos(), tmp.getYPos() - 1);
			}
			if(tmp.getYPos() + tmp.getWidth()/2 < c.getHeight()/2)
			{
				tmp.setPos(tmp.getXPos(), tmp.getYPos() + 1);
			}
		}
	}
	@SuppressWarnings("unused")
	private void expandRect(Canvas c)
	{
		KnowledgeObject tmp = objectToRender;
		
		for(float f = tmp.getWidth(); f < c.getWidth(); f+=1)
		{
			tmp.setSize(f, tmp.getHeight() + 1 );
			if(tmp.getXPos() + tmp.getWidth()/2 > c.getWidth()/2)
			{
				tmp.setPos(tmp.getXPos() - 1, tmp.getYPos());
			}
			if(tmp.getXPos() + tmp.getWidth()/2 < c.getWidth()/2)
			{
				tmp.setPos(tmp.getXPos() + 1, tmp.getYPos());
			}
			if(tmp.getYPos() + tmp.getHeight()/2 > c.getHeight()/2)
			{
				tmp.setPos(tmp.getXPos(), tmp.getYPos() - 1);
			}
			if(tmp.getYPos() + tmp.getWidth()/2 < c.getHeight()/2)
			{
				tmp.setPos(tmp.getXPos(), tmp.getYPos() + 1);
			}
		}
	}
}
