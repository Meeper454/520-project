package com.example.objecttester;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.annotations.Expose;

import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.Log;

public class KnowledgeObject {
	
	
	protected Render renderer;
	public RectF topLeft, topRight, bottomRight, bottomLeft;
	private boolean selected = false;
	@Expose public Frame internalFrame;
	public Frame parentFrame;
	protected final float dragOffset = 25.0f; 
	private boolean scaling;
	private int lastScale;	
	@Expose
	protected HashMap<String, Object> properties;
	public TextBox tb;
	boolean initialized = false;
	
	public KnowledgeObject()
	{
		properties = new HashMap<String, Object>();
	}
	public KnowledgeObject(Frame parent)
	{
		parentFrame = parent;
		properties = new HashMap<String, Object>();
	}
	public KnowledgeObject(Frame parent, HashMap<String, Object> newproperties)
	{
		if(parent != null)
		{
			internalFrame = new Frame(parent, parent.getHolder(), this);		
			if(internalFrame != null)
			parent.addChild(this.internalFrame);
		}
		else
		{
			internalFrame = new Frame(null, null, this);
		}
		parentFrame = parent;
		properties = newproperties;
		initialized = true;
		updateDragDetectors();
		if(!newproperties.containsKey("lock"))
			set("lock", false);
		renderer = new Render(this);
	}
	public KnowledgeObject(Frame parent, PointF loc, double w, double h, KOType objType)
	{		
		internalFrame = new Frame(parent, parent.getHolder(), this);		
		parent.addChild(this.internalFrame);		
		parentFrame = parent;
		//set up drag detectors		
		populateHashMap(loc,w,h,objType);
		initialized = true;
		updateDragDetectors();
		set("text", "");
		set("lock", false);
		renderer = new Render(this);
	}
	
	public boolean hasItem(String key)
	{
		return properties.containsKey(key);
	}
	@SuppressWarnings("unchecked")
	public ArrayList<String> getLinks()
	{
		ArrayList<?> linkObj = (ArrayList<?>)get("links");
		if(linkObj == null)
			return null;
		boolean checked = true;
		for(Object inner: linkObj)
		{
			checked &= inner instanceof String;			
		}
		if(checked)
			return (ArrayList<String>)linkObj;
		else
			return null;
		
	}
	public void addLink (String linkId)
	{
		boolean exists = false;
		ArrayList<String> links = getLinks();
		if(links == null)
		{
			links = new ArrayList<String>();
		}
		for(KnowledgeObject ko: parentFrame.objects)
		{
			String id = (String)ko.get("id");
			exists |= linkId.equals(id);			
		}
		if(exists)
			links.add(linkId);
		set("links", links);
	}
	public void removeLink(String linkId)
	{
		ArrayList<String> links = getLinks();
		if(links != null)
		{
			links.remove(linkId);
			set("links", links);
		}
	}
	public boolean removeProperty(String key)
	{
		return (properties.remove(key) != null);		
	}
	private String generateId()
	{
		String id = "";
		if(parentFrame.getParentKO() == null)
		{
			id = "root" + getNextIdNo();
		}
		else
		{
			id = (String)parentFrame.getParentKO().get("id");
			id += "child" + getNextIdNo();
		}
		return id;
	}
	public HashMap<String, Object> getProperties()
	{
		return properties;
	}
	private int getNextIdNo()
	{		
		ArrayList<KnowledgeObject> siblings = parentFrame.objects;
		ArrayList<Integer> ids = new ArrayList<Integer>();
		for(KnowledgeObject ko: siblings)
		{
			String fullId = (String)ko.get("id");
			int idInFrame = 0;
			String[] idNos = fullId.trim().split("\\D+");
			try
			{
				idInFrame = Integer.parseInt(idNos[idNos.length - 1]);
				if(!ko.equals(this))
					ids.add(idInFrame);		
			}
			catch(Exception ex)
			{
				//We don't care about this id if it doesn't match the pattern.
				//Ignore it.
			}
				
		}
		int newId = -1;
		boolean foundLowestId = false;
		while(!foundLowestId)
		{
			newId++;
			foundLowestId=!ids.contains(newId);			
		}
		return newId;
		
	}
	
	public void setScaling(boolean b)
	{
		scaling = b;
	}
	
	public void expand(Canvas c)
	{
		renderer.expandMe(c);
	}
	public boolean isSelected()
	{
		return selected;
	}
	public Object get(String hash)
	{
		if(parentFrame != null)
		{
			synchronized (parentFrame.getHolder().getThread().mRunLock)
			{
				return properties.get(hash);
			}
		}
		/* No frame, so not being rendered. No need to synchronize*/
		else
			return properties.get(hash);
	}
	
	private void doSet(String key, Object obj)
	{
		if(parentFrame != null)
		{
			synchronized (parentFrame.getHolder().getThread().mRunLock)
			{
				properties.put(key, obj);
			}
		}
		/* No frame, so not being rendered. No need to synchronize*/
		else
			properties.put(key, obj);
			
	}
	/*
	 * Generic setter.
	 * WARNING: DOES NOT UPDATE NECESSARY VALUES FOR CHANGING TYPE
	 * Use setType when changing type.
	 */
	public void set(String key, Object obj)
	{
		Log.d("KO", "Setting " + key + " as " + obj.toString());
		if(properties.containsKey("lock"))
		{
			boolean lock = (Boolean)get("lock");
			if(lock == true)
			{
				Log.d("KO", "OBJECT LOCKED");
				if(key.equalsIgnoreCase("lock"))
				{
					doSet(key, obj);
				}
			}
			else
			{
				if((key.equals("width") || key.equals("height")) && (KOType)get("type") == KOType.KO_CIRCLE)
				{
					boolean keyIsWidth = key.equals("width");
					if(keyIsWidth == true)
					{
						doSet("height", obj);				
					}
					else
					{
						doSet("width", obj);
					}
					doSet(key, obj);
				}				
				else
					doSet(key, obj);
			}
		}
		else
		{
			if((key.equals("width") || key.equals("height")) && (KOType)get("type") == KOType.KO_CIRCLE)
			{
				boolean keyIsWidth = key.equals("width");
				if(keyIsWidth == true)
				{
					doSet("height", obj);				
				}
				else
				{
					doSet("width", obj);
				}
				doSet(key, obj);
			}			
			else
				doSet(key, obj);	
		}
		
		if(initialized)
			updateDragDetectors();
		if(key.equals("zindex"))
			parentFrame.sortByZ();
		//parentFrame.getHolder().space.fm.saveFile(parentFrame.getHolder().base, parentFrame.getHolder().getContext());
	}	
	private void populateHashMap(PointF loc, double w, double h, KOType objType)
	{
		properties = new HashMap<String, Object>();
		//set("renderPos", loc);
		set("x", loc.x);
		set("y", loc.y);
		set("width", w);
		set("height", h);
		set("type", objType);
		set("color", "white");
		set("dropshadow", false);
		set("strokewidth", (int)10);
		set("opacity", (int)255);
		set("id", generateId());
		set("fontsize", (float)80.0);
	}
	private void updateDragDetectors()
	{
		float y = getYPos();
		float x = getXPos();
		float width = getWidth();
		float height = getHeight();
		topLeft = new RectF(x - dragOffset, y - dragOffset, x + dragOffset, y + dragOffset); 
		topRight = new RectF(x + (float)width - dragOffset, y - dragOffset, x + dragOffset + (float)width, y + dragOffset); 
		bottomRight = new RectF(x + (float)width - dragOffset, y + (float)height - dragOffset, x + (float)width + dragOffset, y + (float)height + dragOffset); 
		bottomLeft = new RectF(x - dragOffset, y + (float)height - dragOffset, x + dragOffset, y + (float)height + dragOffset);

		if(tb != null)
		{
			tb.setX(x);
			tb.setY(y + (float)height + 50);
		}

	}
	public void scale(RectF draggedFrom, PointF endPoint)
	{
		if(draggedFrom.equals(topLeft))
		{
			topLeftScale(endPoint);
		}
		else if(draggedFrom.equals(topRight))
		{
			topRightScale(endPoint);
		}
		else if (draggedFrom.equals(bottomLeft))
		{
			botLeftScale(endPoint);
		}
		else if (draggedFrom.equals(bottomRight))
		{
			botRightScale(endPoint);
		}
	}
	/*
	 * Method for determining which drag rectangle has been touched, if any.
	 * Return 0 for no rectangle
	 * Return 1 for top left
	 * Return 2 for top right
	 * Return 3 for bottom right
	 * Return 4 for bottom left
	 */
	public int cornerDrag(float x, float y)
	{
		if(topLeft.contains(x, y))
			{
				Log.v("KO", "Scaling topLeft");
				lastScale = 1;
				return 1;
			}
		else if(topRight.contains(x, y))
			{
				Log.v("KO", "Scaling topRight");
				lastScale = 2;
				return 2;
			}
		else if(bottomRight.contains(x, y))
			{
				Log.v("KO", "Scaling bottomRight");
				lastScale = 3;
				return 3;
			}
		else if(bottomLeft.contains(x, y))
			{
				Log.v("KO", "Scaling bottomLeft");
				lastScale = 4;
				return 4;
			}
		else if(scaling)
			{
				Log.v("KO", "Scaling last: " + lastScale);
				return lastScale;
			}
		return 0;
	}
	public void topLeftScale(PointF endPoint)
	{	
		//Log.v("KO", "Width = " + width);
		//Log.v("KO", "Height = " + height);
		//PointF renderPos = (PointF)get("renderPos");
		float x = (Float)get("x");
		float y = (Float)get("y");
		double width = (Double)get("width");
		double height = (Double)get("height");
		if(width <= 0 || height <= 0)
		{
			if(width <= 0)
			{
				RectF tmp = topLeft;
				topLeft = topRight;
				topRight = tmp;	
				//renderPos.x = renderPos.x + (float)width;
				set("width", -width);
				topRightScale(endPoint);
			}
			if(height <= 0)
			{
				RectF tmp = topLeft;
				topLeft = bottomLeft;
				bottomLeft = tmp;	
				//renderPos.y = renderPos.y + (float)height;
				set("height", -height);
				botLeftScale(endPoint);
			}			
		}	
		else
			{
				set("width", width + (double)(x - endPoint.x));		
				set("height", height + (double)(y - endPoint.y));
				move(endPoint.x, endPoint.y);
			}
					
	}
	public void botLeftScale(PointF endPoint)
	{
		float x = (Float)get("x");
		float y = (Float)get("y");
		double width = (Double)get("width");
		double height = (Double)get("height");
		if(width <= 0 || height <= 0)
		{
			if(width <= 0)
			{
				RectF tmp = bottomLeft;
				bottomLeft = bottomRight;
				bottomRight = tmp;	
				//renderPos.x = renderPos.x + (float)width;
				set("width", -width);
				botRightScale(endPoint);
			}
			if(height <= 0)
			{
				RectF tmp = bottomLeft;
				bottomLeft = topLeft;
				topLeft = tmp;	
				//renderPos.y = renderPos.y + (float)height;
				set("height", -height);
				topLeftScale(endPoint);
			}			
		}
		else
		{
			set("height",  (double)(endPoint.y - y));
			set("width",  width + (double)(x - endPoint.x));
			move(endPoint.x, y);
		}
	}
	public void topRightScale(PointF endPoint)
	{
		float x = (Float)get("x");
		float y = (Float)get("y");
		double width = (Double)get("width");
		double height = (Double)get("height");
		if(width <= 0 || height <= 0)
		{
			if(width <= 0)
			{
				RectF tmp = topRight;
				topRight = topLeft;
				topLeft = tmp;	
				//renderPos.x = renderPos.x + (float)width;
				set("width", -width);
				topLeftScale(endPoint);
			}
			if(height <= 0)
			{
				RectF tmp = topRight;
				topRight = bottomRight;
				bottomRight = tmp;	
				//renderPos.y = renderPos.y + (float)height;
				set("height", -height);
				botRightScale(endPoint);
			}			
		}
		else
		{
			set("width", (double)(endPoint.x -x));
			set("height",  height + (double)(y - endPoint.y));
			move(x, endPoint.y);
		}
	}
	public void botRightScale(PointF endPoint)
	{
		float x = (Float)get("x");
		float y = (Float)get("y");
		double width = (Double)get("width");
		double height = (Double)get("height");
		if(width <= 0 || height <= 0)
		{
			if(width <= 0)
			{
				RectF tmp = bottomRight;
				bottomRight = bottomLeft;
				topRight = tmp;	
				//renderPos.x = renderPos.x + (float)width;
				set("width", -width);
				botLeftScale(endPoint);
			}
			if(height <= 0)
			{
				RectF tmp = bottomRight;
				bottomRight = topRight;
				topRight = tmp;	
				//renderPos.y = renderPos.y + (float)height;
				set("height", -height);
				topRightScale(endPoint);
			}			
		}
		set("width", (double)(endPoint.x -x));
		set("height", (double)(endPoint.y - y));		
		//Force location update for drag rectangles
		move(x, y);
	}
	
	public void setMoving()
	{
		renderer.addRenderer("moving");
	}
	public void destroy()
	{		
			parentFrame.removeLinks(this);
			if(internalFrame == null)
			{

			}
			else if(internalFrame.objects.size() > 0)
			{
				for(KnowledgeObject ko : internalFrame.objects)
				{
					ko.destroy();
				}
			}		
			renderer.destroy();
			properties.clear();
			renderer = null;				
	}
	public void selectMe()
	{
		if(!selected)
		{
			renderer.addRenderer("select");
			selected = true;
			Log.d("FrameSpace", "Selected");
		}		
	}
	public void deSelectMe()
	{
		if(selected)
		{
			renderer.removeRenderer("select");
			selected = false;
			Log.d("FrameSpace", "Deselected");
		}
	}
	/*@Override
	public KnowledgeObject clone()
	{
		KnowledgeObject toReturn = new KnowledgeObject(renderPos, width, height, type);
		toReturn.renderer = renderer.clone(toReturn);
		return toReturn;
	}
	*/
	public  void render(Canvas c) 
	{		
		renderer.renderAll(c);		
	}
	public  void derender(Canvas c) {
	}
	public  void move(float xPos, float yPos) 
	{
		setPos(xPos, yPos);
		updateDragDetectors();
	}
	public  void setSize(float w, float h) 
	{
		setWidth(w);
		setHeight(h);
	}
	
	public float getXPos()
	{
		/*PointF p = (PointF)get("renderPos");
		float x = p.x;
		p = null;*/
		return (Float)get("x");
	}
	public float getYPos()
	{
		/*PointF p = (PointF)get("renderPos");
		float y = p.y;
		p = null;*/
		return (Float)get("y");
	}
	public void setPos(float x, float y)
	{
		set("x", x);
		set("y", y);
	}
	public float getWidth()
	{
        double dv = (Double)get("width");
        float ret = (float)dv;
        return ret;
	}
	public void setWidth(float w)
	{
		set("width",(double)w);
	}	
	public float getHeight()
	{
        double dv = (Double)get("height");
        float ret = (float)dv;
        return ret;
	}
	public void setHeight(float h)
	{
		set("height",(double)h);
	}
	public KOType getType()
	{
		return (KOType)get("type");
	}
	public void setType(KOType toset)
	{
		KOType type = (KOType)get("type");					
		//String text = (String) get("text");		
		
		if(type == KOType.KO_TEXT && toset != KOType.KO_TEXT)
		{			
			Log.d("KO", "Reinstating textrenderer");
			renderer.addRenderer("text");
		}
		if(toset == KOType.KO_TEXT)
		{
			Log.d("KO", "Removing textrenderer");		
			renderer.removeRenderer("text");
			float width = getWidth();
			float height = getHeight();
			set("tWidth", width);
			set("tHeight", height);
		}
		if(type == KOType.KO_TEXT)
		{
			if(properties.containsKey("tWidth"))
			{
				float width = (Float)properties.remove("tWidth");
				float height = (Float)properties.remove("tHeight");
				setWidth(width);
				setHeight(height);
			}	
		}		
		set("type", toset);
		
	}
	public String getText()
	{
		if(get("text") instanceof String)
		{
			return (String)get("text");
		}
		else
			return "";
	}
	public void setText(String textIn)
	{
		set("text",textIn);
	}
	public RectF getRect()
	{
		float x = (Float)get("x");
		float y = (Float)get("y");
		double width = (Double)get("width");
		double height = (Double)get("height");
		RectF toReturn =  new RectF();
		toReturn.set(x, y,(float)(x + width), (float)(y + height));
		return toReturn;
	}
	public void toggleShadow()
	{
		renderer.toggleShadow();
	}

	public void spawnTextInput(TextBox textBox) {
		tb = textBox;
		//renderer.addRenderer("textbox");
		
	}	
	public void destroyTextBox() {
		renderer.removeRenderer("textbox");
		tb = null;		
	}
}
