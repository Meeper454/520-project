package com.example.objecttester;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.AlertDialog;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.Log;


public class Parser 
{
	//private String propertyDictionary[] = {"text", "width", "height", "x", "y", "dropshadow", "color", "strokewidth", "opacity", "id"};	
	private String commandDictionary[] = {"properties", "addlink", "removelink", "remove", "links"};
	private Frame owner;
	private boolean validated = false;
	public Parser(Frame _owner)
	{
		owner = _owner;
	}
	//	private int indexOfProperty(String input)
	//	{
	//		for(int i = 0; i < propertyDictionary.length; i++)
	//		{
	//			if(input.contains(propertyDictionary[i]))
	//				return i;
	//		}
	//		return -1;
	//	}
	private boolean koContainsProperty(KnowledgeObject ko, String property)
	{
		return (ko.get(property) != null);
	}
	private boolean anyKoHasProperty(String property)
	{
		for(KnowledgeObject ko: owner.getSelected())
		{
			if(koContainsProperty(ko, property))
			{
				return true;
			}
		}
		return false;
	}
	public KnowledgeObject search(Frame start, String id)
	{		
		ArrayList<KnowledgeObject> objectList = start.objects;
		KnowledgeObject toReturn = null;
		for(KnowledgeObject ko: objectList)
		{
			if(ko == null)
				Log.wtf("Parser", "Ko is null!");
			if(id == null)
				Log.wtf("Parser", "id is null!");
			if(ko.get("id") == null)
				Log.wtf("Parser", "Ko ID is null!");
			if(ko.get("id").equals(id))
			{
				return ko;
			}
			toReturn = search(ko.internalFrame, id);
		}
		return toReturn;
	}
	public boolean validateProperty(String input)
	{
		Log.d("Parser", "Validating input");
		Pattern regex = Pattern.compile("@\\w+[:=][#\\w\\s]*");
		Matcher m = regex.matcher(input);		
		if(!m.find())
		{
			Log.d("Parser", "Invalid");
			return false;
		}		
		validated = true;
		Log.d("Parser", "Valid = " + validated);
		return validated;
	}
	public boolean validateCommand(String input)
	{		
		Log.d("Parser", "Validating input");
		Pattern regex = Pattern.compile("\\*\\w+[:]\\w+");
		Matcher m = regex.matcher(input);		
		m.find();				
		validated = (indexOfCommand(input) > -1);
		if(!validated)
		{
			regex = Pattern.compile("\\*\\w+");
			m = regex.matcher(input);		
			validated = m.find();
		}		
		Log.d("Parser", "Valid = " + validated);
		return validated;
	}
	private int indexOfCommand(String input)
	{
		for(int i = 0; i < commandDictionary.length; i++)
		{
			if(input.contains(commandDictionary[i]))
				return i;
		}
		return -1;
	}
	public void setPropertiesOfGroup(ArrayList<KnowledgeObject> group, String input)
	{
		for(KnowledgeObject ko : group)
		{
			String property = input.split("[=:]")[0].substring(1).toLowerCase(Locale.ENGLISH);
			String value = input.substring(property.length() + 2);
			Log.d("Parser", property + " being set to: " + value);
			Object propertyObj;
			synchronized(owner.getHolder().getThread().mRunLock)
			{
				propertyObj = ko.get(property);
			}
			if(propertyObj!= null)
			{
				if(propertyObj instanceof String)						
				{
					ko.set(property, value);	
					Log.d("Parser", property + " set to string: " + value);
				}
				else if(propertyObj instanceof Double)
				{
					try
					{
						ko.set(property, Double.parseDouble(value));
						Log.d("Parser", property + " set to double: " + value);
					}
					catch(Exception ex)
					{
						Log.e("Error", "Value not a double");									
					}
				}
				else if(propertyObj instanceof Float)
				{
					try
					{
						ko.set(property, Float.parseFloat(value));
						Log.d("Parser", property + " set to float: " + value);						
					}
					catch(Exception ex)
					{
						Log.e("Error", "Value not a float");									
					}
				}
				else if(propertyObj instanceof Integer)
				{
					try
					{
						Log.d("Parser", property + " set to int: " + value);
						ko.set(property, Integer.parseInt(value));
					}
					catch(Exception ex)
					{
						Log.e("Error", "Value not an integer");
					}
				}
				else if(propertyObj instanceof Boolean)
				{
					try
					{
						ko.set(property, Boolean.parseBoolean(value));
					}
					catch(Exception ex)
					{
						Log.e("Error", "Value not an integer");									
					}
				}
				else if (propertyObj instanceof KOType)
				{					
					if(value.contains("circle"))
					{
						ko.setType(KOType.KO_CIRCLE);
					}
					else if(value.contains("square"))
					{
						ko.setType(KOType.KO_SQUARE);
					}
					else if(value.contains("text"))
					{
						ko.setType(KOType.KO_TEXT);
					}					
				}
			}
			else
			{
				try
				{
					float fValue = Float.parseFloat(value);
					ko.set(property, fValue);
					Log.d("Parser", "New property " + property + " set to: " + value);
				}
				catch(NumberFormatException e)
				{
					ko.set(property, value);
					Log.d("Parser", "New property " + property + " set to: " + value);
				}
			}
		}
	}
	public boolean executeProperty(String input)
	{
		//Should validate each execute.
		//Input not matching the pattern is treated as "@text=" + input
		Log.d("Parser", "Validating");
		validated = validateProperty(input);				

		if(owner.getSelected().size() > 0)
		{
			if(validated)
			{			
				setPropertiesOfGroup(owner.getSelected(), input);
				return true;
			}
			else
			{
				return false;
			}				
		}
		else if(validated)
		{
			setPropertiesOfGroup(owner.objects, input);
			return true;
		}
		else
		{
			Log.d("Parser", "Text Creator");
			PointF p = owner.getListener().getLastTouch();
			//p.set(p.x, p.y);
			Paint rp = new Paint();
			rp.setTextSize(35);
			float width = rp.measureText(input);
			KnowledgeObject ko = new KnowledgeObject(owner, p, width, 35, KOType.KO_TEXT);
			ko.setText(input);
			owner.addObject(ko);
			return true;
		}
	}

	public boolean executeCommand(String input)
	{
		if((validated = validateCommand(input))==true)
		{
			Log.d("Parser", "Command detected");
			if(input.contains(commandDictionary[0]))
			{
				Log.d("Parser", "Full property printer");
				if(owner.getSelected().size() == 0)
				{
					if(input.split(":").length > 1)
					{
						String koId = input.split(":")[1];
						//find ko with name
						KnowledgeObject ko = search(owner.getHolder().base, koId);
						//create text object
						//dump ko properties
						if(ko != null)
						{
							String propertyText = "";
							PointF p = owner.getListener().getLastTouch();
							//p.set(p.x, p.y);
							Paint rp = new Paint();
							rp.setTextSize(35);
							float width = rp.measureText(input);
							KnowledgeObject newko = new KnowledgeObject(owner, p, width, 35, KOType.KO_TEXT);
							for (Map.Entry<String, Object> entry : ko.getProperties().entrySet()) 
							{
								String key = entry.getKey();
								Object value = entry.getValue();
								propertyText += key + '=' + value.toString() +'\n';
							}
							newko.setText(propertyText);
							owner.addObject(newko);
							return true;
						}
					}
					else
					{
						String propertyText = "";
						for(KnowledgeObject ko: owner.objects)
						{
							for (Map.Entry<String, Object> entry : ko.getProperties().entrySet()) 
							{
								String key = entry.getKey();
								Object value = entry.getValue();
								propertyText += key + "=" + value.toString() +"\n";
							}
							propertyText += "-----------------------------------------------\n";
						}
						// 1. Instantiate an AlertDialog.Builder with its constructor
						AlertDialog.Builder builder = new AlertDialog.Builder(owner.getHolder().space);		
						// 2. Chain together various setter methods to set the dialog characteristics
						builder.setMessage(propertyText).setTitle("Properties List");
						builder.setPositiveButton("OK", null);
						builder.setCancelable(true);
						//builder.create().show();	
						// 3. Get the AlertDialog from create()
						AlertDialog dlgAlert = builder.create();		
						dlgAlert.show();
						return true;
					}
				}
				else
				{					
					String propertyText = "";
					for(KnowledgeObject ko: owner.getSelected())
					{
						for (Map.Entry<String, Object> entry : ko.getProperties().entrySet()) 
						{
							String key = entry.getKey();
							Object value = entry.getValue();
							propertyText += key + "=" + value.toString() +"\n";
						}
						propertyText += "-----------------------------------------------\n";
					}
					// 1. Instantiate an AlertDialog.Builder with its constructor
					AlertDialog.Builder builder = new AlertDialog.Builder(owner.getHolder().space);		
					// 2. Chain together various setter methods to set the dialog characteristics
					builder.setMessage(propertyText).setTitle("Properties List");
					builder.setPositiveButton("OK", null);
					builder.setCancelable(true);
					//builder.create().show();	
					// 3. Get the AlertDialog from create()
					AlertDialog dlgAlert = builder.create();		
					dlgAlert.show();
					return true;
				}					
			}
			else if(input.contains(commandDictionary[1]))
			{
				//Insert linker here.
				if(owner.getSelected().size() > 0)
				{
					for(KnowledgeObject ko : owner.getSelected())
					{
						String[] inputSplit = input.split(":");
						if(inputSplit.length == 2)
							ko.addLink(inputSplit[1]);
					}
				}
				Log.d("Parser", "Linker");
				return true;
			}
			else if(input.contains(commandDictionary[2]))
			{
				if(owner.getSelected().size() > 0)
				{
					for(KnowledgeObject ko : owner.getSelected())
					{
						String[] inputSplit = input.split(":");
						if(inputSplit.length == 2)
							ko.removeLink(inputSplit[1]);
					}
				}
				Log.d("Parser", "Linker");
				return true;
			}
			else if(input.contains(commandDictionary[3]))
			{
				if(owner.getSelected().size() > 0)
				{
					for(KnowledgeObject ko : owner.getSelected())
					{
						String[] inputSplit = input.split(":");
						if(inputSplit.length == 2)
							ko.removeProperty(inputSplit[1]);
					}
				}
				Log.d("Parser", "Remover");
				return true;
			}
			else if(input.contains(commandDictionary[4]))
			{
				String propertyText = "";
				for(KnowledgeObject ko: owner.links)
				{
					for (Map.Entry<String, Object> entry : ko.getProperties().entrySet()) 
					{
						String key = entry.getKey();
						Object value = entry.getValue();
						propertyText += key + "=" + value.toString() +"\n";
					}
					propertyText += "-----------------------------------------------\n";
				}
				// 1. Instantiate an AlertDialog.Builder with its constructor
				AlertDialog.Builder builder = new AlertDialog.Builder(owner.getHolder().space);		
				// 2. Chain together various setter methods to set the dialog characteristics
				builder.setMessage(propertyText).setTitle("Properties List");
				builder.setPositiveButton("OK", null);
				builder.setCancelable(true);
				//builder.create().show();	
				// 3. Get the AlertDialog from create()
				AlertDialog dlgAlert = builder.create();		
				dlgAlert.show();
				return true;
			}
			else if(anyKoHasProperty(input.substring(1)))
			{
				Log.d("Parser", "Arbitrary property printer");
				String propertyText = "";
				for(KnowledgeObject ko: owner.getSelected())
				{
					propertyText += "id: " + ko.get("id") + "\n";
					if(!input.substring(1).equals("id"))
						propertyText += input.substring(1) + ": " + ko.get(input.substring(1).toString());
					propertyText += "\n-----------------------------------------------\n";
				}
				// 1. Instantiate an AlertDialog.Builder with its constructor
				AlertDialog.Builder builder = new AlertDialog.Builder(owner.getHolder().space);		
				// 2. Chain together various setter methods to set the dialog characteristics
				builder.setMessage(propertyText).setTitle("Properties Window");
				builder.setPositiveButton("OK", null);
				builder.setCancelable(true);
				//builder.create().show();	
				// 3. Get the AlertDialog from create()
				AlertDialog dlgAlert = builder.create();		
				dlgAlert.show();
				return true;
			}
			else
			{
//				AlertDialog.Builder builder = new AlertDialog.Builder(owner.getHolder().space);		
//				// 2. Chain together various setter methods to set the dialog characteristics
//				builder.setMessage("Invalid property").setTitle("Properties Window");
//				builder.setPositiveButton("OK", null);
//				builder.setCancelable(true);
//				//builder.create().show();	
//				// 3. Get the AlertDialog from create()
//				AlertDialog dlgAlert = builder.create();		
//				dlgAlert.show();
				return false;
			}
		}
		return false;
	}	
}
