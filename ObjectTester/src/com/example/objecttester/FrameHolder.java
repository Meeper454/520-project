package com.example.objecttester;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


@SuppressLint("ClickableViewAccessibility")
public class FrameHolder extends SurfaceView implements SurfaceHolder.Callback
{
	Frame renderFrame;
	Frame base;
	FrameSpace space;
	public Clipboard clipBoard;
	
	private FrameThread thread;	
	
	public void terminateThread()
	{
		//thread.interrupt();
		thread.setRunning(false);
	}
	public void createThread()
	{
		thread = new FrameThread(getHolder(), getContext(), new  Handler(), this);
		//thread.start();
	}
	public FrameHolder(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		SurfaceHolder holder = getHolder();
		holder.addCallback(this);				
		thread = new FrameThread(holder, context, new  Handler(), this);		
	}
	public FrameHolder (FrameSpace fs)
	{
		super(fs.contentView.getContext());
		space = fs;
		SurfaceHolder holder = getHolder();
		holder.addCallback(this);				
		thread = new FrameThread(holder, fs.contentView.getContext(), new  Handler(), this);	
	}
	public void setupFrame(FrameSpace fs)
	{
		space = fs;		
		//childFrames = new ArrayList<Frame>(5);
		renderFrame = new Frame(null, this, null);
		base = renderFrame;
		this.setOnTouchListener(renderFrame.getListener());
		//this.setOnKeyListener(listener);
		this.setOnLongClickListener(renderFrame.getListener());
		clipBoard = new Clipboard(this);
	}
	
	public void swapFrame(Frame swapTo)
	{
		if(swapTo == null)
			Log.wtf("FrameHolder", "Swapping to invalid frame");		
		swapTo.accessFrame(renderFrame);
		//swapTo.getParentKO().expand(thread.mRunLock);
		renderFrame = swapTo;
		this.setOnTouchListener(renderFrame.getListener());
		this.setOnLongClickListener(renderFrame.getListener());
		return;
	}
	
	@SuppressWarnings("unused")
	class FrameThread extends Thread
	{
		//private Bitmap frameCanvas;
		
		private long mLastTime;
		private SurfaceHolder mSurfaceHolder;
		private Handler mHandler;
		private Context mContext;
		private FrameHolder parent;
		public Canvas drawCanvas;
		
		/** Indicate whether the surface has been created & is ready to draw */
        private boolean mRun = false;

        public final Object mRunLock = new Object();
		
		
		public FrameThread(SurfaceHolder holder, Context context, Handler handler, FrameHolder parentFrame) 
		{   // TODO Auto-generated constructor stub
			mSurfaceHolder = holder;
			mHandler = handler;
			mContext = context;
			parent = parentFrame;
		}		
		public void run()
		{
			while (mRun) {
                Canvas c = null;
                try {
                    c = mSurfaceHolder.lockCanvas(null);
                    drawCanvas = c;
                    synchronized (mSurfaceHolder) {
                    	synchronized(mRunLock)
                    	{                   		
                    		if(mRun) doDraw(c);
                        }
                    }
                } finally {
                    // do this in a finally so that if an exception is thrown
                    // during the above, we don't leave the Surface in an
                    // inconsistent state
                    if (c != null) {                    	
                        mSurfaceHolder.unlockCanvasAndPost(c);
                        parent.requestFocus();
                    }
                }
            }
			Log.d("Thread", "Thread closing");
		}
		/**
         * Used to signal the thread whether it should be running or not.
         * Passing true allows the thread to run; passing false will shut it
         * down if it's already running. Calling start() after this was most
         * recently called with false will result in an immediate shutdown.
         *
         * @param b true to run, false to shut down
         */
        public void setRunning(boolean b) {
            // Do not allow mRun to be modified while any canvas operations
            // are potentially in-flight. See doDraw().
            synchronized (mRunLock) {
                mRun = b;
            }
        }
        private void doDraw(Canvas canvas) 
        {
        	canvas.drawColor(Color.WHITE);
        	renderFrame.renderAll(canvas);
        }
		public void setSurfaceSize(int width, int height) {
			// TODO Auto-generated method stub
			synchronized (mSurfaceHolder) {
                // don't forget to resize the background image
                //frameCanvas = Bitmap.createScaledBitmap(frameCanvas, width, height, true);
            }
			
		}
        
	}
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		getThread().setSurfaceSize(width, height);
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		Log.d("FrameHolder", "Trying to start thread");
		getThread().setRunning(true);
		try
        {
			getThread().start();
        }
		catch(Exception ex)
		{
			
		}
		
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		boolean retry = true;
        getThread().setRunning(false);
        while (retry) 
        {
            try {
                getThread().join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
		
	}	
	public FrameThread getThread() {
		return thread;
	}
}
