package com.example.objecttester;

import java.util.ArrayList;
import java.util.HashMap;

import android.graphics.Canvas;
import android.graphics.RectF;

public class Link extends KnowledgeObject
{	
	public Link(Frame parent, HashMap<String, Object> newproperties) 
	{
		parentFrame = parent;
		properties = newproperties;
		initialized = true;
		updateDragDetectors();
		if(!newproperties.containsKey("lock"))
			set("lock", false);
		renderer = new Render(this);	
	}
	public Link (Frame parent, String linkFrom, String linkTo)
	{
		super(parent);
		populateHashMap(linkFrom, linkTo);
		renderer = new Render(this);
	}
	@Override
	public void set(String key, Object entry)
	{
		synchronized (parentFrame.getHolder().getThread().mRunLock)
		{
			properties.put(key, entry);
		}	
	}	
	private void updateDragDetectors()
	{
		float x = getXPos();
		float y = getYPos();
		float width = getWidth();
		float height = getHeight();
		topLeft = new RectF(x - dragOffset, y - dragOffset, x + dragOffset, y + dragOffset); 
		topRight = new RectF(x + width - dragOffset, y - dragOffset, x + dragOffset + width, y + dragOffset); 
		bottomRight = new RectF(x + width - dragOffset, y + height - dragOffset, x + width + dragOffset, y + height + dragOffset); 
		bottomLeft = new RectF(x - dragOffset, y + height - dragOffset, x + dragOffset, y + height + dragOffset);
		
		if(tb != null)
		{
			tb.setX(x);
			tb.setY(y + height + 50);
		}
	}
	public float[] getCoords()
	{		
		
		KnowledgeObject linkFrom = resolveKoName(getLinkedFrom());
		KnowledgeObject linkTo = resolveKoName(getLinkedTo());	
		if(linkFrom != null && linkTo != null)
		{
			float startX, startY, stopX, stopY, a1, a2, b1, b2;
			a1 = linkFrom.getXPos() + linkFrom.getWidth()/2;
			a2 = linkTo.getXPos() + linkTo.getWidth()/2;
			b1 = linkFrom.getYPos() + linkFrom.getHeight()/2;
			b2 = linkTo.getYPos() + linkTo.getHeight()/2;
			double dx1 = a2 - a1;
			double dy1 = b2 - b1;
			double angle1 = Math.atan2(dy1, dx1);

			double dx2 = linkFrom.getXPos() - linkTo.getXPos();
			double dy2 = linkFrom.getYPos() - linkTo.getYPos();
			double angle2 = Math.atan2(dy2, dx2);								

			startX = (float) (a1 + (linkFrom.getWidth()/2 * Math.cos(angle1)));					
			startY = (float) (b1 + (linkFrom.getHeight()/2 * Math.sin(angle1)));					
			stopX = (float) (a2 + (linkTo.getWidth()/2 * Math.cos(angle2)));					
			stopY = (float) (b2 + (linkTo.getHeight()/2 * Math.sin(angle2)));
			float[] coords = {startX, startY, stopX, stopY};
			return coords;
		}
		float[] coords = {0,0,0,0};
		return coords;
	}
	@Override
	public float getWidth()
	{
		float[] coords = getCoords();
		float x1 = coords[0];
		float x2 = coords[2];		
		return Math.abs(x1 - x2);
	}
	@Override
	public float getHeight()
	{
		float[] coords = getCoords();
		float y1 = coords[1];
		float y2 = coords[3];		
		return Math.abs(y1 - y2);
	}
	@Override
	public float getXPos()
	{
		float[] coords = getCoords();
		float x1 = coords[0];
		float x2 = coords[2];
		return Math.min(x1, x2);
	}
	@Override
	public float getYPos()
	{
		float[] coords = getCoords();
		float y1 = coords[1];
		float y2 = coords[3];
		return Math.min(y1, y2);
	}
	@Override
	public RectF getRect()
	{
		float[] coords = getCoords();
		float startX = coords[0];
		float startY = coords[1];
		float stopX = coords[2];
		float stopY = coords[3];
		
		RectF rect = new RectF();
		float left, top, right, bottom;
		
		left = (startX < stopX)? startX : stopX; 
		right = (startX > stopX)? startX : stopX;
		top = (startY < stopY)? startY : stopY; 
		bottom = (startY > stopY)? startY : stopY; 
		rect.set(left, top, right, bottom);		
		return rect;	
	}
	private KnowledgeObject resolveKoName(String koName) 
	{
		ArrayList<KnowledgeObject> siblings = parentFrame.objects;
		for(KnowledgeObject ko : siblings)
		{
			if(ko.get("id").toString().equals(koName))
			{
				return ko;
			}
		}
		return null;
	}
	public void populateHashMap(String linkFrom, String linkTo)
	{
		set("type", KOType.KO_LINK);
		set("linkTo", linkTo);
		set("linkFrom", linkFrom);
		set("color", "black");
		set("dropshadow", false);
		set("strokewidth", (int)10);
		set("opacity", (int)255);
		updateDragDetectors();
	}
	@Override
	public int cornerDrag(float x, float y)
	{
		return -1;
	}
	public String getLinkedFrom()
	{
		return (String)get("linkFrom");
	}
	public String getLinkedTo()
	{
		return (String)get("linkTo");
	}
	@Override
	public  void render(Canvas c) 
	{		
		updateDragDetectors();
		renderer.renderAll(c);		
	}
	public void destroy()
	{
		
	}
}
