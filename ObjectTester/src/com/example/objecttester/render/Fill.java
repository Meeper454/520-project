package com.example.objecttester.render;

import java.util.ArrayList;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.Paint.Style;
import android.util.Log;

import com.example.objecttester.KOType;
import com.example.objecttester.KnowledgeObject;
import com.example.objecttester.Link;
import com.example.objecttester.Render;

public class Fill extends Render
{
	public Fill(KnowledgeObject ko) 
	{
		objectToRender = ko;	
		rType = renderType.RT_FILL;
	}
	private KnowledgeObject resolveKoName(String koName)
	{
		ArrayList<KnowledgeObject> siblings = objectToRender.parentFrame.objects;
		for(KnowledgeObject ko : siblings)
		{
			if(ko.get("id").toString().equals(koName))
			{
				return ko;
			}
		}
		return null;
	}	
	public void renderMe(Canvas c) 
	{
		// TODO Auto-generated method stub
		Paint p = new Paint();
		String scolor = (String)objectToRender.get("color");
		int color = Color.WHITE;		
		boolean shadow = (Boolean)objectToRender.get("dropshadow");
		int opacity = (Integer)objectToRender.get("opacity");
				
		try
			{
				color = Color.parseColor(scolor);
			}
			catch(Exception ex)
			{
				Log.e("Render", "Color not recognised");
			}
		p.setColor(color);
		
		if(shadow)
			p.setShadowLayer(5, 0, 5, Color.argb(128,0,0,0));		
		
		p.setStyle(Style.FILL);
		p.setAlpha(opacity);
		switch(objectToRender.getType())
		{
			case KO_CIRCLE:
				doCircle(c, p);
				break;
			case KO_SQUARE:				
				doSquare(c, p);
				break;
			case KO_TEXT:
				doText(c,p);
				break;
			case KO_LINK:
				doLink(c,p);
				break;
			default:
				break;
		}
	}
	private void doLink(Canvas c, Paint p) 
	{
		Link toRender = (Link)objectToRender;
		int strokewidth = 10;
		KnowledgeObject linkTo = resolveKoName(toRender.getLinkedTo());
		if(toRender.hasItem("strokewidth"))
			strokewidth = (Integer)toRender.get("strokewidth");
		p.setStrokeWidth(strokewidth);
		if(linkTo != null && linkTo.getType() == KOType.KO_CIRCLE)
		{			
			float coords[] = toRender.getCoords();				
			float startX = coords[0];
			float startY = coords[1];
			float stopX = coords[2];
			float stopY = coords[3];
			c.drawLine(startX, startY, stopX, stopY, p);
		}
	}
	private void doCircle(Canvas c, Paint p)
	{
		float radius;		
		if(objectToRender.getWidth() < objectToRender.getHeight())
			radius = objectToRender.getWidth()/2;
		else
			radius = objectToRender.getHeight()/2;		
		
		c.drawCircle(objectToRender.getXPos() + radius, objectToRender.getYPos() + radius, radius, p);
	}
	private void doSquare(Canvas c, Paint p)
	{
		c.drawRect(objectToRender.getXPos(), objectToRender.getYPos(), objectToRender.getXPos() + objectToRender.getWidth(), objectToRender.getYPos() + objectToRender.getHeight(), p);		
	}	
	private void doText(Canvas c, Paint p) 
	{
		p.setTextSize(100);
		p.setTextScaleX(1.0f);
		
	    //Rect textBounds = new Rect();
	    
	    if(objectToRender.getText() != null)
	  
	    {  	//Log.d("FrameSpace", "TO_TEXT: " + objectToRender.getText());
	    	//renderPaint.getTextBounds(objectToRender.getText(), 0, objectToRender.getText().length(), textBounds);
	    
	    	
	    	if(!objectToRender.hasItem("fontsize"))
	    	{
	    		float hi=100, lo=2, targetWidth=objectToRender.getWidth();
	    	float threshold = 0.5f;
	    	while((hi - lo) > threshold) {
	            float size = (hi+lo)/2;
	            p.setTextSize(size);
	            if(p.measureText(objectToRender.getText()) >= targetWidth) 
	                hi = size; // too big
	            else
	                lo = size; // too small
	        }	 
	    }
	    	else
	    	{	float fontsize = (Float)objectToRender.get("fontsize");
	    		p.setTextSize(fontsize);
	    	}
	    	String typefamily = "sans-serif";
			String typestyle = "normal";
			int typestyleno;
			Typeface typeface;
			if(objectToRender.hasItem("typeface"))
				typefamily = (String)objectToRender.get("typeface");
			if(objectToRender.hasItem("typestyle"))
				typestyle = (String)objectToRender.get("typestyle");

            if(typestyle.equals("bold"))
            {
                typestyleno = Typeface.BOLD;
            }
            else if(typestyle.equals("italic"))
            {
                typestyleno = Typeface.ITALIC;
            }
            else if(typestyle.equals("bold_italic"))
            {
                typestyleno = Typeface.BOLD_ITALIC;
            }
            else
            {
                typestyleno = Typeface.NORMAL;
            }
			typeface = Typeface.create(typefamily, typestyleno);
			int color = Color.BLACK;
			String scolor = (String)objectToRender.get("textcolor");
			if(objectToRender.hasItem("textcolor"))
			{
				try
				{
					color = Color.parseColor(scolor);
				}
				catch(Exception ex)
				{
					Log.e("Render", "Color not recognised");
				}
			}
		    p.setTypeface(typeface);
		    p.setColor(color);
	    	String text = objectToRender.getText();
	    	Rect bounds = new Rect();
	    	p.getTextBounds(text, 0, text.length(), bounds);
	    	Rect objRect = new Rect();
	    	objRect.set((int)objectToRender.getXPos(), (int)objectToRender.getYPos(),(int) objectToRender.getXPos() + (int)objectToRender.getWidth(), (int)objectToRender.getYPos() + (int)objectToRender.getHeight());
	    	float w = objectToRender.getWidth(), h = objectToRender.getHeight();
	    	float tw = 100, th = 100;
	    	if(objectToRender.hasItem("tWidth"))
	    		{
	    			tw = (Float)objectToRender.get("tWidth");
	    			th = (Float)objectToRender.get("tHeight");
	    		}
	    	boolean set = false;
	    	if((text.equals("") || text.equals(" ")))
	    	{
	    		if(w != tw || h!= th)
	    			objectToRender.setSize(tw, th);
	    		
	    		set = true;
	    	}
	    	else if(w != bounds.width() + bounds.width()/20 || h != bounds.height())
	    	{
	    		if(!set)
	    			objectToRender.setSize(bounds.width() + bounds.width()/20, bounds.height());
	    	}
	    	
	    	float drawY = objectToRender.getHeight() + bounds.height();
	    	drawY/=2;
	    	drawY+=objectToRender.getYPos();	    	
			c.drawText(text, objectToRender.getXPos(), drawY, p);
	    }
	}
}
