package com.example.objecttester.render;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;

import com.example.objecttester.KnowledgeObject;
import com.example.objecttester.Render;

public class InnerText extends Render{
	
	private Paint renderPaint = new Paint();
	protected renderType rType;

	public InnerText(KnowledgeObject ko) {
		// TODO Auto-generated constructor stub
		objectToRender = ko;
		rType = renderType.RT_INNERTEXT;
	}
	@Override
	public InnerText clone()
	{
		return new InnerText(objectToRender);
	}
	public renderType getType()
	{
		return rType;
	}
	@Override
	public void renderMe(Canvas c) {
		renderPaint.setColor(Color.BLACK);
		String text = objectToRender.getText();
		
		renderPaint.setTextSize(100);
		renderPaint.setTextScaleX(1.0f);
	    //Rect textBounds = new Rect();	    

    	if(!objectToRender.hasItem("fontsize"))
    	{
    		float hi=100, lo=2, targetWidth=objectToRender.getWidth();
    	float threshold = 0.5f;
    	while((hi - lo) > threshold) {
            float size = (hi+lo)/2;
            renderPaint.setTextSize(size);
            if(renderPaint.measureText(objectToRender.getText()) >= targetWidth) 
                hi = size; // too big
            else
                lo = size; // too small
        	}	 
    	}
    	else
    	{
    		if(objectToRender.get("fontsize") instanceof String)
    		{
    			float newV = Float.parseFloat((String)objectToRender.get("fontsize"));
    			objectToRender.set("fontsize", newV);
    		}
    		
    		float fontsize = (float)(Float)objectToRender.get("fontsize");
    		renderPaint.setTextSize(fontsize);
    	}
    	
    	String typefamily = "sans-serif";
		String typestyle = "normal";
		int typestyleno;
		Typeface typeface;
		if(objectToRender.hasItem("typeface"))
			typefamily = (String)objectToRender.get("typeface");
		if(objectToRender.hasItem("typestyle"))
			typestyle = (String)objectToRender.get("typestyle");


		if(typestyle.equals("bold"))
        {
            typestyleno = Typeface.BOLD;
        }
        else if(typestyle.equals("italic"))
        {
            typestyleno = Typeface.ITALIC;
        }
        else if(typestyle.equals("bold_italic"))
        {
            typestyleno = Typeface.BOLD_ITALIC;
        }
		else
        {
            typestyleno = Typeface.NORMAL;
        }

		typeface = Typeface.create(typefamily, typestyleno);
		int color = Color.BLACK;
		if(objectToRender.hasItem("textcolor"))
		{
			color = Color.parseColor((String)objectToRender.get("textcolor"));
		}
	    renderPaint.setTypeface(typeface);
	    renderPaint.setColor(color);
//	    // get the height that would have been produced
//	    int h = textBounds.bottom - textBounds.top;
//	 
//	    // make the text text up 70% of the height
//	    float target = (float)objectToRender.getHeight()*.7f;
//	 
//	    // figure out what textSize setting would create that height
//	    // of text
//	    float size  = ((target/h)*100f);
//	    renderPaint.setTextSize(size);	
//	    // and set it into the paint	    
//	 // do calculation with scale of 1.0 (no scale)
//	    renderPaint.setTextScaleX(1.0f);
//	    
//	    renderPaint.getTextBounds(objectToRender.getText(), 0, objectToRender.getText().length(), textBounds);
//	    // determine the width
//	    int w = textBounds.right - textBounds.left;
//	 	 
//	    float targetWidth = objectToRender.getWidth()-(objectToRender.getWidth()/40)*2;
//	    // determine how much to scale the width to fit the view
//	    float xscale = targetWidth/w;
//	    //Log.d("Render", "xscale = " + xscale + ", w = " + w +", objectW = " + objectToRender.getWidth());
//	    //renderPaint.setTextSize(size * xscale/2);	
//	    // set the scale for the text paint
//	        
//	    renderPaint.setTextScaleX(xscale);	     
		//Log.d("TextBounds", "Drew The text");
    	float objCX =  objectToRender.getWidth()/2;
    	float txCX = renderPaint.measureText(text)/2;
    	float drawX = objectToRender.getXPos() + objCX - txCX;    	
    	Rect bounds = new Rect();
    	renderPaint.getTextBounds(text, 0, text.length(), bounds);
    	float drawY = objectToRender.getHeight() + bounds.height();
    	drawY/=2;
    	drawY+=objectToRender.getYPos();
		c.drawText(objectToRender.getText(), drawX, drawY, renderPaint);
		
	}

}
