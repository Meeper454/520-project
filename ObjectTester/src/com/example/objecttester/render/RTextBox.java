package com.example.objecttester.render;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
//import android.graphics.Rect;

import com.example.objecttester.Render;
import com.example.objecttester.TextBox;

public class RTextBox extends Render 
{
	private final int height = 50;	
	private TextBox objectToRender;
	
	public RTextBox(TextBox textBox) 
	{		
		objectToRender = textBox;
		rType = renderType.RT_TEXTBOX;
	}
	
	public void renderMe(Canvas c)
	{	
		Paint renderPaint = new Paint();
		renderPaint.setTextSize(35);
		renderPaint.setTextScaleX(1.0f);
		renderPaint.setColor(Color.BLACK);
		float x = objectToRender.getX();
    	float y = objectToRender.getY(); 
		String renderText = objectToRender.getText();		
		/*Rect textBounds = new Rect();
		//Log.d("Render", "TB_TEXT: " + renderText);
    	renderPaint.getTextBounds(renderText, 0, renderText.length(), textBounds);
    	 	
    
 
	    // get the height that would have been produced
	    int h = textBounds.bottom - textBounds.top;
	 
	    // make the text text up 70% of the height
	    float target = (float)height*.7f;
	 
	    // figure out what textSize setting would create that height
	    // of text
	    float size  = ((target/h)*100f);
	 
	    // and set it into the paint
	    renderPaint.setTextSize(size); */   
	    
	    renderPaint.setStyle(Style.STROKE);
	    c.drawRect(x,y/*-35*/, x+renderPaint.measureText(renderText), y/*-35*/+height, renderPaint);
	    renderPaint.setStyle(Style.FILL_AND_STROKE);
	    c.drawText(renderText, x, y + renderPaint.getTextSize(), renderPaint);
	}
}
