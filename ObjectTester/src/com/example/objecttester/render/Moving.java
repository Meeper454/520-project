package com.example.objecttester.render;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Paint.Style;

import com.example.objecttester.KnowledgeObject;
import com.example.objecttester.Render;

public class Moving extends Render{
	
	protected renderType rType;
	private final int textBeadRadius = 10;
	private final int textBeadSpace = 10;
	public Moving(KnowledgeObject ko)
	{
		objectToRender = ko;
		rType = renderType.RT_MOVE;
	}
	public Moving clone()
	{
		return null;
	}
	public renderType getType()
	{
		return rType;
	}
	public void renderMe(Canvas c)
	{
		switch(objectToRender.getType())
		{
			case KO_SQUARE:
				square(c);
				break;
			case KO_CIRCLE:
				circle(c);
				break;
			case KO_TEXT:
				text(c);
				break;
			default:
				break;
		}
	}
	private void circle(Canvas c) {
		// TODO Auto-generated method stub
		float radius;
		if(objectToRender.getWidth() < objectToRender.getHeight())
			radius = objectToRender.getWidth()/2;
		else
			radius = objectToRender.getHeight()/2; 
		Paint p = new Paint();
		p.setStrokeWidth(10);
		p.setColor(Color.argb(128,0,0,0));
		p.setStyle(Style.STROKE);
		c.drawCircle(objectToRender.getXPos() + radius, objectToRender.getYPos() + radius, radius, p);
	}

	private void text(Canvas c) 
	{
		// TODO Auto-generated method stub
		Paint renderPaint = new Paint();
		renderPaint.setColor(Color.argb(128,0,0,0));
		
		
		renderPaint.setTextSize(100);
		renderPaint.setTextScaleX(1.0f);
	    Rect textBounds = new Rect();
	    
	    if(objectToRender.getText() != null)	  
	    {
	    	renderPaint.getTextBounds(objectToRender.getText(), 0, objectToRender.getText().length(), textBounds);
	    
	 
		    // get the height that would have been produced
		    int h = textBounds.bottom - textBounds.top;
		 
		    // make the text text up 70% of the height
		    float target = (float)objectToRender.getHeight()*.7f;
		 
		    // figure out what textSize setting would create that height
		    // of text
		    float size  = ((target/h)*100f);
		 
		    // and set it into the paint
		    renderPaint.setTextSize(size);
		    
		    // do calculation with scale of 1.0 (no scale)
		    renderPaint.setTextScaleX(1.0f);
		    
		    // determine the width
		    int w = textBounds.right - textBounds.left;
		 	 
		    // determine how much to scale the width to fit the view
		    float xscale = objectToRender.getWidth() / w;
		 
		    // set the scale for the text paint
		    renderPaint.setTextScaleX(xscale);
		    
			//Log.d("TextBounds", "Drew The text");
			c.drawText(objectToRender.getText(), objectToRender.getXPos() + objectToRender.getWidth()/20 + textBeadRadius + textBeadSpace, objectToRender.getYPos() + (0.75f) * objectToRender.getHeight(), renderPaint);
		
	    }
	}

	private void square(Canvas c)
	{
		Paint renderPaint = new Paint();
		renderPaint.setColor(Color.argb(128,0,0,0));
		renderPaint.setStyle(Style.STROKE);		
		renderPaint.setStrokeWidth(10);		
		c.drawRect(objectToRender.getXPos(), objectToRender.getYPos(), objectToRender.getXPos() + objectToRender.getWidth(), objectToRender.getYPos() + objectToRender.getHeight(), renderPaint);
	}
}
