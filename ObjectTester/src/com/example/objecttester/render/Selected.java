package com.example.objecttester.render;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import com.example.objecttester.KnowledgeObject;
import com.example.objecttester.Render;

public class Selected extends Render{

	private Paint outlinePaint = new Paint();
	private final int Offset = 10;
	protected renderType rType;
	
	public Selected(KnowledgeObject ko)
	{
		objectToRender = ko;
		rType = renderType.RT_SELECT;
	}
	public renderType getType()
	{
		return rType;
	}
	@Override
	public Selected clone()
	{
		return null;
	}
	@Override
	public void renderMe(Canvas c) {
		// TODO Auto-generated method stub
		outlinePaint.setColor(Color.CYAN);		
		outlinePaint.setStyle(Style.FILL);
		//float radius = 35;
				
		c.drawRect(objectToRender.bottomLeft, outlinePaint);
		c.drawRect(objectToRender.bottomRight, outlinePaint);
		c.drawRect(objectToRender.topLeft, outlinePaint);
		c.drawRect(objectToRender.topRight, outlinePaint);
		
		
//		c.drawCircle(objectToRender.getXPos() - Offset, objectToRender.getYPos() - Offset, radius, outlinePaint);
//		c.drawCircle(objectToRender.getXPos() + objectToRender.getWidth() + Offset, objectToRender.getYPos() - Offset, radius, outlinePaint);
//		c.drawCircle(objectToRender.getXPos() - Offset, objectToRender.getYPos() + objectToRender.getHeight() + Offset, radius, outlinePaint);
//		c.drawCircle(objectToRender.getXPos() + objectToRender.getWidth() + Offset, objectToRender.getYPos() + objectToRender.getHeight() + Offset, radius, outlinePaint);
//		
		outlinePaint.setStrokeWidth(5);
		outlinePaint.setStyle(Style.STROKE);
		c.drawRect(objectToRender.getXPos() - Offset, objectToRender.getYPos() - Offset, objectToRender.getXPos() + objectToRender.getWidth() + Offset, objectToRender.getYPos() + objectToRender.getHeight() + Offset, outlinePaint);		
		
		//radius = 0.75f * radius;		
		outlinePaint.setColor(Color.BLACK);
		c.drawRect(objectToRender.bottomLeft, outlinePaint);
		c.drawRect(objectToRender.bottomRight, outlinePaint);
		c.drawRect(objectToRender.topLeft, outlinePaint);
		c.drawRect(objectToRender.topRight, outlinePaint);
		
//		c.drawCircle(objectToRender.getXPos() - Offset, objectToRender.getYPos() - Offset, radius, outlinePaint);
//		c.drawCircle(objectToRender.getXPos() + objectToRender.getWidth() + Offset, objectToRender.getYPos() - Offset, radius, outlinePaint);
//		c.drawCircle(objectToRender.getXPos() - Offset, objectToRender.getYPos() + objectToRender.getHeight() + Offset, radius, outlinePaint);
//		c.drawCircle(objectToRender.getXPos() + objectToRender.getWidth() + Offset, objectToRender.getYPos() + objectToRender.getHeight() + Offset, radius, outlinePaint);		
//		
			}

}
