package com.example.objecttester.render;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;

import com.example.objecttester.KnowledgeObject;
import com.example.objecttester.Render;

public class DropShadow extends Render{
	
	private final float offset = 5;
	protected renderType rType;
	public DropShadow(KnowledgeObject ko)
	{
		objectToRender = ko;
		rType = renderType.RT_SHADOW;
	}
	public renderType getType()
	{
		return rType;
	}
	@Override
	public DropShadow clone()
	{
		return new DropShadow(objectToRender);
	}
	public void renderMe(Canvas c)
	{
		switch(objectToRender.getType())
		{
			case KO_SQUARE:
				square(c);
				break;
			case KO_CIRCLE:
				circle(c);
				break;
			case KO_TEXT:
				text(c);
				break;
			default:
				break;
		}
	}
	private void circle(Canvas c) {
		// TODO Auto-generated method stub
		
	}

	private void text(Canvas c) {
		// TODO Auto-generated method stub
		
	}

	private void square(Canvas c)
	{
		Paint renderPaint = new Paint();
		renderPaint.setColor(Color.WHITE);
		renderPaint.setStyle(Style.FILL_AND_STROKE);		
		renderPaint.setStrokeWidth(10);
		renderPaint.setShadowLayer(offset, 0, offset, Color.argb(128,0,0,0));
		c.drawRect(objectToRender.getXPos(), objectToRender.getYPos(), objectToRender.getXPos() + objectToRender.getWidth(), objectToRender.getYPos() + objectToRender.getHeight(), renderPaint);
	}

}
