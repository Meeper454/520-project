package com.example.objecttester.render;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
//import android.util.Log;

import com.example.objecttester.KnowledgeObject;
import com.example.objecttester.Render;

public class Outline extends Render 
{
	//protected renderType rType;
	public Outline(KnowledgeObject ko) 
	{
		objectToRender = ko;	
		rType = renderType.RT_OUTLINE;
	}
	
	private void doCircle(Canvas c, Paint p)
	{
		float radius;		
		if(objectToRender.getWidth() < objectToRender.getHeight())
			radius = objectToRender.getWidth()/2;
		else
			radius = objectToRender.getHeight()/2;		
		
		c.drawCircle(objectToRender.getXPos() + radius, objectToRender.getYPos() + radius, radius, p);
	}
	private void doSquare(Canvas c, Paint p)
	{
		c.drawRect(objectToRender.getXPos(), objectToRender.getYPos(), objectToRender.getXPos() + objectToRender.getWidth(), objectToRender.getYPos() + objectToRender.getHeight(), p);		
	}	
	public void renderMe(Canvas c) {
		// TODO Auto-generated method stub
		Paint p = new Paint();
		int strokewidth = (Integer)objectToRender.get("strokewidth");
		boolean shadow = (Boolean)objectToRender.get("dropshadow");
		int opacity = (Integer)objectToRender.get("opacity");
		int color = Color.BLACK;
		if(shadow)
			p.setShadowLayer(5, 0, 5, Color.argb(128,0,0,0));
		if(objectToRender.hasItem("strokecolor"))
		{
			try
			{
				String scolor = (String)objectToRender.get("strokecolor");
				color = Color.parseColor(scolor);
			}
			catch(Exception ex)
			{
				
			}
		}
		p.setColor(color);
		p.setStrokeWidth(strokewidth);		
		p.setStyle(Style.STROKE);	
		p.setAlpha(opacity);			
		switch(objectToRender.getType())
		{
			case KO_CIRCLE:
				doCircle(c, p);
				break;
			case KO_SQUARE:				
				doSquare(c, p);
				break;
			default:
				break;
		}		
	}
}
