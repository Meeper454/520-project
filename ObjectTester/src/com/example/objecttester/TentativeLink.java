package com.example.objecttester;

import java.util.ArrayList;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.Log;

public class TentativeLink 
{
	private KnowledgeObject linkFrom;
	private PointF endPoint;
	private PointF touchPoint;
	private KnowledgeObject lastOnLine;
	private KnowledgeObject onLine;
	
	public TentativeLink(KnowledgeObject start, float finishX, float finishY)
	{
		linkFrom = start;
		touchPoint = new PointF(finishX, finishY);
		endPoint = touchPoint;
	}
	public void renderMe(Canvas c) 
	{
		Paint p = new Paint();
		p.setColor(Color.BLACK);
		p.setStrokeWidth(10);
		p.setAlpha(128);
		float startX = linkFrom.getXPos() + linkFrom.getWidth()/2;
		float startY = linkFrom.getYPos() + linkFrom.getHeight()/2;
		c.drawLine(startX, startY, endPoint.x, endPoint.y, p);		
	}
	public PointF getEndpoint()
	{
		return endPoint;
	}
	public void extrapolate()
	{
		
		//y = mx + c;
		float x1, x2, y1, y2, dx, dy, m, c;
		
		x1 = linkFrom.getXPos() + linkFrom.getWidth();
		y1 = linkFrom.getYPos() + linkFrom.getHeight()/2;
		x2 = touchPoint.x;
		y2 = touchPoint.y;
		
		dx = x2-x1;
		dy = y2-y1;
		
		m = dy/dx;
		
		// y - mx = c
		c = y1 - m * x1;
		
		ArrayList<PointF> newPoints = new ArrayList<PointF>();
		
		if(dx > 0)
		{
			for(float xT = x1; xT < 2560; xT += 20)
			{
				float yT = m * xT + c;
				PointF newP = new PointF(xT, yT);
				newPoints.add(newP);
				Frame f = linkFrom.parentFrame;
				KnowledgeObject ko;
				if((ko = f.getListener().findKOByCoords(xT, yT)) != null)
				{
					if(ko != linkFrom && ko.getType() == KOType.KO_CIRCLE)
					{
						onLine = ko;
						ko.selectMe();
						endPoint = new PointF(xT, yT);
						xT = 3000;
					}
				}
				else
				{
					onLine = null;
					endPoint = touchPoint;
				}
			}
		}
		else if (dx < 0)
		{
			for(float xT = x1; xT > 0; xT -= 20)
			{
				float yT = m * xT + c;
				PointF newP = new PointF(xT, yT);
				newPoints.add(newP);
				Frame f = linkFrom.parentFrame;
				KnowledgeObject ko;
				if((ko = f.getListener().findKOByCoords(xT, yT)) != null)
				{
					if(ko != linkFrom && ko.getType() == KOType.KO_CIRCLE)
					{
						onLine = ko;
						ko.selectMe();
						endPoint = new PointF(xT, yT);
						xT = 0;
					}
				}
				else
				{
					onLine = null;
					endPoint = touchPoint;
				}
			}
		}
		else if (dx == 0)
		{
			if(dy > 0)
			{
				for(float yT = y1; yT < 2000; yT += 20)
				{

					PointF newP = new PointF(x1, yT);
					newPoints.add(newP);
					Frame f = linkFrom.parentFrame;
					KnowledgeObject ko;
					if((ko = f.getListener().findKOByCoords(x1, yT)) != null)
					{
						if(ko != linkFrom && ko.getType() == KOType.KO_CIRCLE)
						{
							onLine = ko;
							ko.selectMe();
							endPoint = new PointF(x1, yT);
							yT = 0;
						}
					}
					else
					{
						onLine = null;
						endPoint = touchPoint;
					}
				}
			}
			else if(dy < 0)
			{
				for(float yT = y1; yT > 0; yT -= 20)
				{

					PointF newP = new PointF(x1, yT);
					newPoints.add(newP);
					Frame f = linkFrom.parentFrame;
					KnowledgeObject ko;
					if((ko = f.getListener().findKOByCoords(x1, yT)) != null)
					{
						if(ko != linkFrom && ko.getType() == KOType.KO_CIRCLE)
						{
							onLine = ko;
							ko.selectMe();
							endPoint = new PointF(x1, yT);
							yT = 0;
						}
					}
					else
					{
						onLine = null;
						endPoint = touchPoint;
					}
				}
			}
			else
			{
				Log.d("Link","Still on the centre point. Do nothing");
			}
		}
		if(lastOnLine != onLine)
		{
			if(lastOnLine != null)
				lastOnLine.deSelectMe();
			lastOnLine = onLine;
		}
		if(onLine == null)
		{
			if(lastOnLine != null)
				lastOnLine.deSelectMe();
		}
	}
	public void setEndpoint(PointF newE)
	{
		synchronized(linkFrom.parentFrame.getHolder().getThread().mRunLock)
		{
			touchPoint = newE;
		}
	}
	public KnowledgeObject getLinkKo()
	{
		return onLine;
	}

}
