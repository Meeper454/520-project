package com.example.objecttester;


import java.util.ArrayList;

import com.example.objecttester.FrameHolder.FrameThread;
import com.example.objecttester.util.SystemUiHider;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
@SuppressLint("ClickableViewAccessibility")
@SuppressWarnings("unused")
public class FrameSpace extends Activity {
	/**
	 * Whether or not the system UI should be auto-hidden after
	 * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
	 */
	private static final boolean AUTO_HIDE = false;

	/**
	 * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
	 * user interaction before hiding the system UI.
	 */
	//private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

	/**
	 * If set, will toggle the system UI visibility upon interaction. Otherwise,
	 * will show the system UI visibility upon interaction.
	 */
	private static final boolean TOGGLE_ON_CLICK = false;

	/**
	 * The flags to pass to {@link SystemUiHider#getInstance}.
	 */
	//private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

	/**
	 * The instance of the {@link SystemUiHider} for this activity.
	 */
	//private SystemUiHider mSystemUiHider;
	
	private String bootMsg = "";
	private String dir = "";
	private String fn = "";
	//@SuppressWarnings("unused")
	FrameHolder contentView;
	FileManager fm = new FileManager();
public void generateBootMsg()
{
	bootMsg += "- Working \n";
		bootMsg += "\tCreation. (Draw a circle and it makes a circle.) \n ";
		bootMsg += "\tObject type changing. (Touch an object with two fingers and it will change type) \n ";
		bootMsg += "\tMovement (Drag an object and it�ll move. Threshold set) \n ";
		bootMsg += "\tSelect/Deselect (Touch an object and it�ll get selected. Touching blank space or another object and it�ll deselect the last selected item) \n";
		bootMsg += "\tObject rendering as directed. (Square, Circle, Text, Internal Text, Selection, and Drop Shadow renderers implemented) \n";
		bootMsg += "\tScaling (Shape flipping may cause undesired behviour.\n";
		bootMsg += "\tObject deletion. (Pressing delete key while item is selected will delete the item)\n";
		bootMsg += "\tState persistence. Objects are now saved upon modification and reloaded on up startup\n";
		bootMsg += "\tFixed circle scaling. Circles can no longer have cases of width>height or vice versa\n";
		bootMsg += "\tCtrl-A now selects everything on screen.\n";
	bootMsg += "-------------------------------------------------------------------------------------------------------\n";
	bootMsg += "- Partially Implemented\n";
		bootMsg += "\tFramespace switching (No animation) \n";
		bootMsg += "\t Text input (Ugly)\n";
		
	bootMsg += "-------------------------------------------------------------------------------------------------------\n";
	bootMsg += "- Planned\n";
	
		
}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d("FrameSpace", "Starting");
		super.onCreate(savedInstanceState);

		generateStartedDialog();
		
		setContentView(R.layout.activity_frame_space);
		
		final View controlsView = findViewById(R.id.fullscreen_content_controls);
		Log.d("FrameSpace", "Getting frame from XML");		
		contentView = (FrameHolder) findViewById(R.id.startFrame);
		Log.d("FrameSpace", "Got frame");		
		contentView.setupFrame(this);
		if(contentView.renderFrame.objects == null)
			Log.wtf("FrameSpace", "Frame set to null???!?!?!?");
		
		Frame f = fm.loadFile(this, contentView);		
		contentView.base = f;
		Log.d("FrameSpace", "Tried to load first frame from file");
		if(f != null)
			{
				contentView.renderFrame = f;
				contentView.setOnTouchListener(f.getListener());
				contentView.setOnLongClickListener(f.getListener());
			}
		else
		{
			Log.d("FrameSpace", "Creating points");
			PointF sp = new PointF();
			sp.x = contentView.getLeft()+100;
			sp.y = contentView.getTop()+150;
			PointF cp = new PointF();
			cp.x = 500;
			cp.y = 500;
			PointF tp = new PointF();
			tp.x = 300;
			tp.y = 300;
			PointF ssp = new PointF();
			ssp.x = 1000;
			ssp.y = 1000;
			Log.d("FrameSpace", "Creating Working Knowledge Objects");
			KnowledgeObject so = new KnowledgeObject(contentView.renderFrame, sp, 200.0, 200.0, KOType.KO_SQUARE);
			KnowledgeObject co = new KnowledgeObject(contentView.renderFrame, cp, 200.0, 200.0, KOType.KO_CIRCLE);		
			KnowledgeObject to = new KnowledgeObject(contentView.renderFrame, tp, 500.0, 100, KOType.KO_TEXT);
			KnowledgeObject ss = new KnowledgeObject(contentView.renderFrame, ssp, 200f, 200f, KOType.KO_SQUARE);		
			Log.d("FrameSpace", "Creating Untested Knowledge Objects");
			Log.d("FrameSpace", "Setting display text");
			to.setText("Some really, really, really long string");
			Log.d("FrameSpace", "Adding knowledge objects to frame");
			contentView.renderFrame.addObject(so);
			contentView.renderFrame.addObject(co);
			contentView.renderFrame.addObject(to);
			contentView.renderFrame.addObject(ss);
		}
		
				
	}
	private void generateStartedDialog()
	{
		generateBootMsg();
		// 1. Instantiate an AlertDialog.Builder with its constructor
		AlertDialog.Builder builder = new AlertDialog.Builder(this);		
		// 2. Chain together various setter methods to set the dialog characteristics
		builder.setMessage(bootMsg).setTitle("Feature List");
		builder.setPositiveButton("OK", null);
		builder.setCancelable(true);
		//builder.create().show();	
		// 3. Get the AlertDialog from create()
		AlertDialog dlgAlert = builder.create();		
		dlgAlert.show();		
		dlgAlert.getWindow().setLayout(1800, 1200);
	}
		
		
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		// Trigger the initial hide() shortly after the activity has been
		// created, to briefly hint to the user that UI controls
		// are available.		
		
		Log.d("FrameSpace", "Objects added. We're done here");
	}	
	private void createFileNameInputDialog(Context c)
	{
		 AlertDialog.Builder builder = new AlertDialog.Builder(c);
	        builder.setTitle("Enter Filename");        
	        final EditText input = new EditText(c);
	        input.setInputType(InputType.TYPE_CLASS_TEXT);
	        builder.setView(input);
	        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() { 
	            @Override
	            public void onClick(DialogInterface dialog, int which) {
	                fn = input.getText().toString();
	                fm.saveExternal(contentView.renderFrame, dir + '/' + fn);
	                Toast.makeText(FrameSpace.this, "Saved to: " + dir + '/' + fn, Toast.LENGTH_LONG).show();            
	                
	            }
	        });
	        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	            @Override
	            public void onClick(DialogInterface dialog, int which) {
	                dialog.cancel();
	            }
	        });
	        builder.show();
	}
	public void saveExternalFile()
	{
		Log.d("FileManager", "Opening Save Dialog");
		SaveExporter directoryChooserDialog = 
                new SaveExporter(FrameSpace.this, 
                    new SaveExporter.ChosenDirectoryListener() 
                {
                    @Override
                    public void onChosenDir(String chosenDir) 
                    {
                       dir = chosenDir;                       
                       createFileNameInputDialog(FrameSpace.this);                                   
                    }
                }); 
                // Toggle new folder button enabling
                directoryChooserDialog.setNewFolderEnabled(true);
                // Load directory chooser dialog for initial 'm_chosenDir' directory.
                // The registered callback will be called upon final directory selection.
                directoryChooserDialog.chooseDirectory(dir);                
	}
	public void loadExternalFile()
	{
		Log.d("FileManager", "Opening Save Dialog");
		LoadImporter directoryChooserDialog = 
                new LoadImporter(FrameSpace.this, 
                    new LoadImporter.ChosenDirectoryListener() 
                {
                    @Override
                    public void onChosenDir(String chosenDir) 
                    {
                       dir = chosenDir;                       
                       Frame in = fm.loadExternal(contentView.renderFrame ,dir);                       
                       Frame parent = contentView.renderFrame.getLastParent();
                       ArrayList<Frame> parents = new ArrayList<Frame>();
                       parents.add(parent);
                       //General case. Update the references.
                       if(parent != contentView.base)
                       {
                    	   contentView.renderFrame.getParentKO().internalFrame = in;
                    	   parent.addChild(in);
                    	   in.parentFrames = parents;                   	   	                   	   	
                       }
                       //Potential special case if base is parent.
                       else if(parent == contentView.base)
                       {
                    	   //Base is parent of the current frame, continue as normal
                    	   if(contentView.base != contentView.renderFrame)
                    	   { 
                    		   contentView.renderFrame.getParentKO().internalFrame = in;                    		  
                        	   parent.addChild(in);
                        	   in.parentFrames = parents;                    	   	                   	   	
                           }
                    	   //Current frame is the base frame, so set the base to the incoming frame.
                    	   else
                    		   {
                    		   	contentView.base = in;
                    		   	in.parentFrames = null;
                    		   }
                       }
                       contentView.renderFrame = in;
                       FrameListener fl = in.getListener();
                       contentView.setOnTouchListener(fl);
               		   contentView.setOnLongClickListener(fl);
                       fl.deselectAll();
                    	   
                       Log.d("FileManager", "Loaded into : " + contentView.renderFrame.getId());
                    }
                }); 
                // Toggle new folder button enabling
                directoryChooserDialog.setNewFolderEnabled(true);
                // Load directory chooser dialog for initial 'm_chosenDir' directory.
                // The registered callback will be called upon final directory selection.
                directoryChooserDialog.chooseDirectory(dir);                
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event)
	{
		return contentView.renderFrame.getListener().onKeyUp(keyCode, event);
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		return contentView.renderFrame.getListener().onKeyDown(keyCode, event);
	}
	@Override
	protected void onPause() 
	{
		Log.d("FrameSpace", "Pausing");
		super.onPause();
		contentView.terminateThread();
	}
	@Override
	protected void onStop() 
	{
		Log.d("FrameSpace", "Stopping");
		super.onStop();
		contentView.terminateThread();
	}
	@Override
	protected void onResume() 
	{
		Log.d("FrameSpace", "Resuming");
		super.onResume();
		contentView.createThread();
	}
	@Override
	protected void onRestart()
	{
		Log.d("FrameSpace", "Restarting");
		super.onRestart();		
		contentView.createThread();
	}
	
}
