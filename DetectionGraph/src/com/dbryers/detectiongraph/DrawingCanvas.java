package com.dbryers.detectiongraph;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.graphics.*;
import android.graphics.drawable.*;
import android.content.Context;
import android.graphics.drawable.shapes.*;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View.OnClickListener;

@SuppressLint("ClickableViewAccessibility")
@SuppressWarnings("unused")
class DrawingCanvas extends View implements OnClickListener, OnMenuItemClickListener
{
	private GraphCanvas graph;
	private Path drawPath;
	private ArrayList<Point> pathPoints = new ArrayList<Point>();
	private Paint drawPaint, canvasPaint;
	private int paintColor = 0xFF000000;
	private Canvas drawCanvas;
	private Bitmap canvasBitmap;
	private boolean erase=false;
	private float mX, mY, lX, lY;
	private PopupMenu popupMenu;

	public DrawingCanvas(Context context, AttributeSet attrs){
		super(context, attrs);
		setupDrawing();
	}
	
	public void initGraph(GraphCanvas toGraphOn)
	{
		graph = toGraphOn;
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh)
	{
		super.onSizeChanged(w,h,oldw,oldh);
		canvasBitmap= Bitmap.createBitmap(w,h,Bitmap.Config.ARGB_8888);
		drawCanvas = new Canvas(canvasBitmap);
	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		canvas.drawColor(Color.TRANSPARENT);
		canvas.drawBitmap(canvasBitmap,0,0, canvasPaint);
		canvas.drawPath(drawPath, drawPaint);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		float touchX = event.getX();
		float touchY = event.getY();


		switch(event.getAction())
		{
			case MotionEvent.ACTION_DOWN:
				drawPath.reset();
				drawPath.moveTo(touchX, touchY);
				mX=touchX;
				mY=touchY;
				graph.clearGraph();
				invalidate();
				break;
			case MotionEvent.ACTION_MOVE:
				TOUCH_MOVE(touchX, touchY);
				break;
			case MotionEvent.ACTION_UP:
				TOUCH_UP();
				break;
			default:
				return false;
		}

		return true;
	}
	private void TOUCH_MOVE(float x, float y)
	{
		float dx = Math.abs(x - mX);
		float dy = Math.abs(y - mY);
		if(dx>=4 || dy>=4)
		{
			if(erase)
			{
				drawPath.moveTo(mX, mY);
				drawPath.lineTo(x, y);
				drawCanvas.drawPath(drawPath, drawPaint);
				drawPath.reset();
				mX = x;
				mY = y;
			}
			else
			{
				drawPath.quadTo(mX, mY, (x + mX)/2,(mY +y)/2);
				//displayMessage("Point X = " + x + "; Point Y = " + x);
				Point toAdd = new Point((int)mX, (int)mY);
				pathPoints.add(toAdd);
				toAdd = new Point((int)(x + mX)/2, (int)(mY +y)/2);
				pathPoints.add(toAdd);
				mX = x;
				mY = y;
			}
			invalidate();
		}
	}
	private void TOUCH_UP()
	{
		if(!erase)
		{
			drawCanvas.drawPath(drawPath, drawPaint);
			Point c = graph.findCentre(pathPoints);				
			graph.drawGraph(pathPoints);
			String message = PathOps.approximateShape(graph._distancesFromOrigin);
			graph._distancesFromOrigin.clear();
			pathPoints.clear();
			drawPath.reset();
			drawPath.moveTo(c.x, c.y);					
			drawPath.addCircle(c.x, c.y, (float) 1, Path.Direction.CW);					
			//displayMessage(message);
			popCorrectionMenu(message);
		}
		
		drawCanvas.drawPath(drawPath, drawPaint);
		drawPath.reset();				
		invalidate();
	}
	private void popCorrectionMenu(String shape)
	{	
		popupMenu.getMenu().clear();
		if(shape.compareTo("Circle") != 0)
			{
				popupMenu.getMenu().add(Menu.NONE, 0, Menu.NONE, "Square");
				
			}
		else
		{
			popupMenu.getMenu().add(Menu.NONE, 1, Menu.NONE, "Circle");
			
		}
        popupMenu.getMenu().add(Menu.NONE, 2, Menu.NONE, "Cancel");        
        popupMenu.show();
	}
	public void onClick(View view)
	{
		Log.d("poppy", "Popup triggered for some dumb reason");
		return;
	}
	public boolean onMenuItemClick(MenuItem item) {
		 
		switch (item.getItemId()) {
 
		case 0:
			Toast.makeText(this.getContext(), "Corrected to square", Toast.LENGTH_SHORT).show();
			return true;
		case 1:
			Toast.makeText(this.getContext(), "Corrected to circle", Toast.LENGTH_SHORT).show();
			return true;
		case 2:
			Toast.makeText(this.getContext(), "No correction", Toast.LENGTH_SHORT).show();
			return true;
		default:
			return false;			
		}
	}
	private void displayMessage(String message)
	{
		AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this.getContext());
		dlgAlert.setMessage(message);
		dlgAlert.setTitle("App Title");
		dlgAlert.setPositiveButton("OK", null);
		dlgAlert.setCancelable(true);
		dlgAlert.create().show();
	}
	private void setupDrawing()
	{
		drawPaint = new Paint();
		drawPath = new Path();
		drawPaint.setColor(paintColor);
		drawPaint.setAntiAlias(true);
		drawPaint.setStrokeWidth(10);
		drawPaint.setStyle(Paint.Style.STROKE);
		drawPaint.setStrokeJoin(Paint.Join.ROUND);
		drawPaint.setStrokeCap(Paint.Cap.ROUND);
		canvasPaint = new Paint(Paint.DITHER_FLAG);				
		popupMenu = new PopupMenu(this.getContext(), (View)this);
		popupMenu.setOnMenuItemClickListener(this);
		//graph.clearGraph();
	}
	public void setErase(boolean isErase)
	{
		erase=isErase;
		if(erase)
		{
			drawPaint.setColor(Color.WHITE);
			drawPaint.setStrokeWidth(30);
			drawPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));	
		}
		else
		{
			drawPaint.setXfermode(null);
			drawPaint.setColor(Color.BLACK);
			drawPaint.setStrokeWidth(10);
		}
	}
}
