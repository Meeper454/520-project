package com.dbryers.detectiongraph;

import java.util.ArrayList;

import android.graphics.Point;
import android.util.Log;

@SuppressWarnings("unused")

public class PathOps {
	
	public static float getMaxDistFromOrigin(ArrayList<Float> distancesFromOrigin)
	{
		float maxDist = 0;
		for(Float p : distancesFromOrigin)
		{
			if(p > maxDist)
				maxDist = p;
		}
		return maxDist;
	}
	public static float getMinDistFromOrigin(ArrayList<Float> distancesFromOrigin)
	{
		float minDist = Float.MAX_VALUE;
		for(Float p : distancesFromOrigin)
		{
			if(p < minDist)
				minDist = p;
		}
		return minDist;
	}
	public static float getAvgDistFromOrigin(ArrayList<Float> distancesFromOrigin)
	{
		float avg;
		float sum = 0;
		int count = 0;
		
		for(Float p : distancesFromOrigin)
		{
			sum += p;
			count ++;
		}
		avg = sum/count;
		return avg;
	}
	public static String approximateShape(ArrayList<Float> distancesFromOrigin)
	{
		if(is4SidedPolygon(distancesFromOrigin))
			return "This shape is probably a square";
		else if(isCircle(distancesFromOrigin, 0.5))
			return "This shape is a circle";
		else
			return "Undetermined";
	}
	
	private static boolean is4SidedPolygon(ArrayList<Float> distancesFromOrigin)
	{
		float localMaxima = 0;		
		float avg = getAvgDistFromOrigin(distancesFromOrigin);
		boolean maximaReached = false;
		int maximaCounter = 0;
		
		for(Float f : distancesFromOrigin)
		{
			if(f > avg)
			{
				if(f > localMaxima)
				{
					localMaxima = f;
					
				}
				else
				{
					if(!maximaReached)
					{
						Log.d("Polygon", "localMaxima found");
						maximaCounter++;
						maximaReached = true;
					}
				}
			}
			else				
				maximaReached = false;
		}	
		
		return (maximaCounter == 4);
	}
	
	private static float variance(ArrayList<Float> distancesFromOrigin, float avg)
	{
		float sum = 0;
        for (int i = 0; i < distancesFromOrigin.size(); i++) {
            sum += (distancesFromOrigin.get(i) - avg) * (distancesFromOrigin.get(i) - avg);
        }
        return sum / (distancesFromOrigin.size() - 1);
	}
	
	public static boolean isCircle(ArrayList<Float> distancesFromOrigin, double error) 
	{
		float avg = getAvgDistFromOrigin(distancesFromOrigin); 
		Log.d("GraphCanvas", "avg = " + avg);
		long deviations = 0;
		long allowance = Math.round(error * distancesFromOrigin.size());
		float devAmount = (float)(error * avg);
		
		for(float f: distancesFromOrigin)
		{			
			if(Math.abs(f - avg) > devAmount)				
				deviations++;
			Log.d("GraphCanvas", "f-avg =" + Math.abs(f-avg));
		}
		Log.d("GraphCanvas", "Deviations = " + deviations);
		Log.d("GraphCanvas", "Allowance = " + allowance);
		return (deviations < allowance);		
	}
	
	
	/*private static boolean isCircle(ArrayList<Float> distancesFromOrigin)
	{
		ArrayList<Float> weights = new ArrayList<Float>();
		float avg, error, min, max;
		long deviations = 0;		
		//min = getMinDistFromOrigin(distancesFromOrigin);
		//avg = getAvgDistFromOrigin(distancesFromOrigin);
		//max = getMaxDistFromOrigin(distancesFromOrigin);
		
		

		
		float errorMod = (float)Math.sqrt(avg);
		long allowedErrors = Math.round(Math.pow(2.0, (double)avg));
		
		
		for(Float p: distancesFromOrigin)
		{
			if(Math.abs(p-avg) > (error))
				deviations++;
		}
		return (deviations < allowedErrors);
	}*/

}
