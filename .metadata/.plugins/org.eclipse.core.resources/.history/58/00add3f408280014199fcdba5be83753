package com.example.objecttester;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;
import android.view.View.OnTouchListener;

@SuppressWarnings("unused")
public class Frame extends SurfaceView implements SurfaceHolder.Callback, OnTouchListener{
	
	ArrayList<KnowledgeObject> objects;
	ArrayList<Frame> childFrames;
	ArrayList<Frame> parentFrames;	
	
	Frame activeParent;
	
	private boolean movement = false;
	private boolean sameObject = false;
	private float touchX1, touchY1;
	private float touchX2, touchY2;
	private KnowledgeObject touchKo1 = null;
	private KnowledgeObject touchKo2 = null;
	
	private FrameThread thread;
	private Context mContext;
	
	
	public Frame(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		SurfaceHolder holder = getHolder();
		holder.addCallback(this);				
		thread = new FrameThread(holder, context, new  Handler(), objects);		
	}
	public void setupFrame()
	{
		objects = new ArrayList<KnowledgeObject>(10);
		childFrames = new ArrayList<Frame>(5);
	}
	
	/*public void renderKnowledgeObjects()
	{
		for(KnowledgeObject KO : objects)
			KO.render();
	}*/
	class FrameThread extends Thread
	{
		private Bitmap frameCanvas;
		private long mLastTime;
		private SurfaceHolder mSurfaceHolder;
		private Handler mHandler;
		private Context mContext;
		private ArrayList<KnowledgeObject> obs;
		
		/** Indicate whether the surface has been created & is ready to draw */
        private boolean mRun = false;

        private final Object mRunLock = new Object();
		
		
		public FrameThread(SurfaceHolder holder, Context context, Handler handler, ArrayList<KnowledgeObject> objects) 
		{   // TODO Auto-generated constructor stub
			mSurfaceHolder = holder;
			mHandler = handler;
			mContext = context;
		}		
		public void run()
		{
			while (mRun) {
                Canvas c = null;
                try {
                    c = mSurfaceHolder.lockCanvas(null);
                    synchronized (mSurfaceHolder) {
                    	synchronized(mRunLock)
                    	{                   		
                    		if(mRun) doDraw(c);
                        }
                    }
                } finally {
                    // do this in a finally so that if an exception is thrown
                    // during the above, we don't leave the Surface in an
                    // inconsistent state
                    if (c != null) {
                        mSurfaceHolder.unlockCanvasAndPost(c);
                    }
                }
            }
		}
		/**
         * Used to signal the thread whether it should be running or not.
         * Passing true allows the thread to run; passing false will shut it
         * down if it's already running. Calling start() after this was most
         * recently called with false will result in an immediate shutdown.
         *
         * @param b true to run, false to shut down
         */
        public void setRunning(boolean b) {
            // Do not allow mRun to be modified while any canvas operations
            // are potentially in-flight. See doDraw().
            synchronized (mRunLock) {
                mRun = b;
            }
        }
        private void doDraw(Canvas canvas) 
        {
        	canvas.drawColor(Color.WHITE);
        	for(KnowledgeObject KO : objects)
    			KO.render(canvas);
        }
		public void setSurfaceSize(int width, int height) {
			// TODO Auto-generated method stub
			synchronized (mSurfaceHolder) {
                // don't forget to resize the background image
                //frameCanvas = Bitmap.createScaledBitmap(frameCanvas, width, height, true);
            }
			
		}
        
	}
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		thread.setSurfaceSize(width, height);
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		thread.setRunning(true);
        thread.start();
		
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		boolean retry = true;
        thread.setRunning(false);
        while (retry) 
        {
            try {
                thread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
		
	}
	@SuppressLint("ClickableViewAccessibility")
	public boolean onTouch(View v, MotionEvent event)
	{
		Log.d("FrameSpace","Touch Detected");
		if(event.getPointerCount() == 1)
		{
			
			switch(event.getAction())		
			{
				case MotionEvent.ACTION_DOWN:
					break;
				case MotionEvent.ACTION_MOVE:
					break;
				case MotionEvent.ACTION_UP:
					break;				
			}
			return true;
		}
		else if (event.getPointerCount() == 2)
		{
			Log.d("FrameSpace", "2 Pointers detected");
			switch(event.getActionMasked())		
			{
				case MotionEvent.ACTION_DOWN:
					touchX1 = event.getX();
					touchY1 = event.getY();
					touchKo1 = findKOByCoords(touchX1, touchY1);
					break;
				case MotionEvent.ACTION_MOVE:
					break;
				case MotionEvent.ACTION_UP:
				case MotionEvent.ACTION_POINTER_UP:
					if(sameObject)
					{
						KOType types[] = KOType.values();
						KOType obtype = touchKo1.getType();
						KOType newType = types[0];
						for(int i = 0; i < types.length; i++)
						{
							if(types[i].equals(obtype))
							{
								if(i != types.length - 1)
								{
									newType = types[i + 1];
								}
								else
									newType = types[0];
							}
						}
						touchKo1.setType(newType);
						touchKo1 = null; touchKo2 = null;
					}
					break;
				case MotionEvent.ACTION_POINTER_DOWN:
					touchX2 = event.getX();
					touchY2 = event.getY();
					touchKo2 = findKOByCoords(touchX2, touchY2);
					assert(touchKo1 != null && touchKo2 != null);
					sameObject = touchKo1.equals(touchKo2);					
					break;
			}
			return true;
		}
		return false;
		
	}
	public KnowledgeObject findKOByCoords(float x, float y)
	{
		for(KnowledgeObject ko: objects)
		{
			RectF koRect = ko.getRect();
			if(koRect.contains(x,y))
				return ko;			
		}
		return null;		
	}

}
