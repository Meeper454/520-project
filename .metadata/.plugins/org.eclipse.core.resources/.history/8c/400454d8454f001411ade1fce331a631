package com.example.objecttester;

import java.util.HashMap;

import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.Log;

public class KnowledgeObject {
	
	private String text = "";
	private Render renderer;
	public RectF topLeft, topRight, bottomRight, bottomLeft;
	private boolean selected = false;
	public Frame internalFrame;
	public Frame parentFrame;
	private final float dragOffset = 35.0f; 
	private boolean scaling;
	private int lastScale;	
	private HashMap<String, Object> properties;
	public TextBox tb;
	
	public void setScaling(boolean b)
	{
		scaling = b;
	}
	
	public void expand(Canvas c)
	{
		renderer.expandMe(c);
	}
	public boolean isSelected()
	{
		return selected;
	}
	public Object get(String hash)
	{
		synchronized (parentFrame.getHolder().getThread().mRunLock)
		{
			return properties.get(hash);
		}
	}
	/*
	 * Generic setter.
	 * WARNING: DOES NOT UPDATE NECESSARY VALUES FOR CHANGING TYPE
	 * Use setType when changing type.
	 */
	public void set(String key, Object obj)
	{
		synchronized (parentFrame.getHolder().getThread().mRunLock)
		{
			properties.put(key, obj);
		}
		if((key.equals("width") || key.equals("height")) && (KOType)get("type") == KOType.KO_CIRCLE)
		{
			boolean keyIsWidth = key.equals("width");
			if(keyIsWidth == true)
				{
					properties.put("height", obj);				
				}
			else
				properties.put("width", obj);
		}
	}
	public KnowledgeObject(Frame parent, PointF loc, double w, double h, KOType objType)
	{		
		internalFrame = new Frame(parent, parent.getHolder(), this);		
		parent.addChild(this.internalFrame);		
		parentFrame = parent;
		//set up drag detectors		
		populateHashMap(loc,w,h,objType);
		updateDragDetectors();
		set("text", "");
		renderer = new Render(this);
	}
	private void populateHashMap(PointF loc, double w, double h, KOType objType)
	{
		properties = new HashMap<String, Object>();
		set("renderPos", loc);
		set("width", w);
		set("height", h);
		set("type", objType);
		set("color", "white");
		set("dropshadow", false);
		set("strokewidth", (int)10);
		set("opacity", (int)100);
	}
	private void updateDragDetectors()
	{
		PointF renderPos = (PointF)get("renderPos");
		double width = (double)get("width");
		double height = (double)get("height");
		topLeft = new RectF(renderPos.x - dragOffset, renderPos.y - dragOffset, renderPos.x + dragOffset, renderPos.y + dragOffset); 
		topRight = new RectF(renderPos.x + (float)width - dragOffset, renderPos.y - dragOffset, renderPos.x + dragOffset + (float)width, renderPos.y + dragOffset); 
		bottomRight = new RectF(renderPos.x + (float)width - dragOffset, renderPos.y + (float)height - dragOffset, renderPos.x + (float)width + dragOffset, renderPos.y + (float)height + dragOffset); 
		bottomLeft = new RectF(renderPos.x - dragOffset, renderPos.y + (float)height - dragOffset, renderPos.x + dragOffset, renderPos.y + (float)height + dragOffset);
		if((KOType)get("type") == KOType.KO_CIRCLE)
		{						
			if(width > height)
			{
				set("height", width);
			}
			else if(width < height)
			{
				set("width", height);
			}
		}
		if(tb != null)
		{
			tb.setX(renderPos.x);
			tb.setY(renderPos.y + (float)height + 50);
		}
	}
	public void scale(RectF draggedFrom, PointF endPoint)
	{
		if(draggedFrom.equals(topLeft))
		{
			topLeftScale(endPoint);
		}
		else if(draggedFrom.equals(topRight))
		{
			topRightScale(endPoint);
		}
		else if (draggedFrom.equals(bottomLeft))
		{
			botLeftScale(endPoint);
		}
		else if (draggedFrom.equals(bottomRight))
		{
			botRightScale(endPoint);
		}
	}
	/*
	 * Method for determining which drag rectangle has been touched, if any.
	 * Return 0 for no rectangle
	 * Return 1 for top left
	 * Return 2 for top right
	 * Return 3 for bottom right
	 * Return 4 for bottom left
	 */
	public int cornerDrag(float x, float y)
	{
		if(topLeft.contains(x, y))
			{
				Log.v("KO", "Scaling topLeft");
				lastScale = 1;
				return 1;
			}
		else if(topRight.contains(x, y))
			{
				Log.v("KO", "Scaling topRight");
				lastScale = 2;
				return 2;
			}
		else if(bottomRight.contains(x, y))
			{
				Log.v("KO", "Scaling bottomRight");
				lastScale = 3;
				return 3;
			}
		else if(bottomLeft.contains(x, y))
			{
				Log.v("KO", "Scaling bottomLeft");
				lastScale = 4;
				return 4;
			}
		else if(scaling)
			{
				Log.v("KO", "Scaling last: " + lastScale);
				return lastScale;
			}
		return 0;
	}
	public void topLeftScale(PointF endPoint)
	{	
		//Log.v("KO", "Width = " + width);
		//Log.v("KO", "Height = " + height);
		PointF renderPos = (PointF)get("renderPos");
		double width = (double)get("width");
		double height = (double)get("height");
		if(width <= 0 || height <= 0)
		{
			if(width <= 0)
			{
				RectF tmp = topLeft;
				topLeft = topRight;
				topRight = tmp;	
				//renderPos.x = renderPos.x + (float)width;
				set("width", -width);
				topRightScale(endPoint);
			}
			if(height <= 0)
			{
				RectF tmp = topLeft;
				topLeft = bottomLeft;
				bottomLeft = tmp;	
				//renderPos.y = renderPos.y + (float)height;
				set("height", -height);
				botLeftScale(endPoint);
			}			
		}	
		else
			{
				set("width", width + (double)(renderPos.x - endPoint.x));		
				set("height", height + (double)(renderPos.y - endPoint.y));
				move(endPoint.x, endPoint.y);
			}
					
	}
	public void botLeftScale(PointF endPoint)
	{
		PointF renderPos = (PointF)get("renderPos");
		double width = (double)get("width");
		double height = (double)get("height");
		if(width <= 0 || height <= 0)
		{
			if(width <= 0)
			{
				RectF tmp = bottomLeft;
				bottomLeft = bottomRight;
				bottomRight = tmp;	
				//renderPos.x = renderPos.x + (float)width;
				set("width", -width);
				botRightScale(endPoint);
			}
			if(height <= 0)
			{
				RectF tmp = bottomLeft;
				bottomLeft = topLeft;
				topLeft = tmp;	
				//renderPos.y = renderPos.y + (float)height;
				set("height", -height);
				topLeftScale(endPoint);
			}			
		}
		else
		{
			set("height",  (double)(endPoint.y - renderPos.y));
			set("width",  width + (double)(renderPos.x - endPoint.x));
			move(endPoint.x, renderPos.y);
		}
	}
	public void topRightScale(PointF endPoint)
	{
		PointF renderPos = (PointF)get("renderPos");
		double width = (double)get("width");
		double height = (double)get("height");
		if(width <= 0 || height <= 0)
		{
			if(width <= 0)
			{
				RectF tmp = topRight;
				topRight = topLeft;
				topLeft = tmp;	
				//renderPos.x = renderPos.x + (float)width;
				set("width", -width);
				topLeftScale(endPoint);
			}
			if(height <= 0)
			{
				RectF tmp = topRight;
				topRight = bottomRight;
				bottomRight = tmp;	
				//renderPos.y = renderPos.y + (float)height;
				set("height", -height);
				botRightScale(endPoint);
			}			
		}
		else
		{
			set("width", (double)(endPoint.x -renderPos.x));
			set("height",  height + (double)(renderPos.y - endPoint.y));
			move(renderPos.x, endPoint.y);
		}
	}
	public void botRightScale(PointF endPoint)
	{
		PointF renderPos = (PointF)get("renderPos");
		double width = (double)get("width");
		double height = (double)get("height");
		if(width <= 0 || height <= 0)
		{
			if(width <= 0)
			{
				RectF tmp = bottomRight;
				bottomRight = bottomLeft;
				topRight = tmp;	
				//renderPos.x = renderPos.x + (float)width;
				set("width", -width);
				botLeftScale(endPoint);
			}
			if(height <= 0)
			{
				RectF tmp = bottomRight;
				bottomRight = topRight;
				topRight = tmp;	
				//renderPos.y = renderPos.y + (float)height;
				set("height", -height);
				topRightScale(endPoint);
			}			
		}
		set("width", (double)(endPoint.x -renderPos.x));
		set("height", (double)(endPoint.y - renderPos.y));		
		//Force location update for drag rectangles
		move(renderPos.x, renderPos.y);
	}
	
	public void setMoving()
	{
		renderer.addRenderer("moving");
	}
	public void destroy()
	{
		if(internalFrame.objects.size() > 0)
		{
			for(KnowledgeObject ko : internalFrame.objects)
				{
					ko.destroy();
				}
		}
		renderer.destroy();
		properties.clear();
		text = null;
		renderer = null;
		return;
	}
	public void selectMe()
	{
		if(!selected)
		{
			renderer.addRenderer("select");
			selected = true;
			Log.d("FrameSpace", "Selected");
		}		
	}
	public void deSelectMe()
	{
		if(selected)
		{
			renderer.removeRenderer("select");
			selected = false;
			Log.d("FrameSpace", "Deselected");
		}
	}
	/*@Override
	public KnowledgeObject clone()
	{
		KnowledgeObject toReturn = new KnowledgeObject(renderPos, width, height, type);
		toReturn.renderer = renderer.clone(toReturn);
		return toReturn;
	}
	*/
	public  void render(Canvas c) 
	{		
		renderer.renderAll(c);
		//Paint p = new Paint();
		//p.setColor(Color.GREEN);
		
		 //Draws drag detection areas
		  //c.drawRect(topLeft, p);
		  //c.drawRect(topRight, p);
		  //c.drawRect(bottomLeft, p);
		  //c.drawRect(bottomRight, p);
		
	}
	public  void derender(Canvas c) {
	}
	public  void move(float xPos, float yPos) 
	{
		setPos(xPos, yPos);
		updateDragDetectors();
	}
	public  void setSize(float w, float h) {
	}
	
	public float getXPos()
	{
		PointF p = (PointF)get("renderPos");
		float x = p.x;
		p = null;
		return x;
	}
	public float getYPos()
	{
		PointF p = (PointF)get("renderPos");
		float y = p.y;
		p = null;
		return y;
	}
	public void setPos(float x, float y)
	{
		set("renderPos", new PointF(x,y));
	}
	public float getWidth()
	{
		return (float)(double)get("width");
	}
	public void setWidth(float w)
	{
		set("width",w);
	}	
	public float getHeight()
	{
		return (float)(double)get("height");
	}
	public void setHeight(float h)
	{
		set("height",h);
	}
	public KOType getType()
	{
		return (KOType)get("type");
	}
	public void setType(KOType toset)
	{
		KOType type = (KOType)get("type");
		renderer.removeRenderer(type.toString());		
		renderer.removeRenderer("text");
		type = toset;
		renderer.addRenderer(toset.toString());
		if(text != "" && toset != KOType.KO_TEXT)
		{
			renderer.addRenderer("text");
		}
		set("type", type);
		
	}
	public String getText()
	{
		if(get("text") instanceof String)
		{
			return (String)get("text");
		}
		else
			return "";
	}
	public void setText(String textIn)
	{
		set("text",textIn);
	}
	public RectF getRect()
	{
		PointF renderPos = (PointF)get("renderPos");
		double width = (double)get("width");
		double height = (double)get("height");
		RectF toReturn =  new RectF();
		toReturn.set(renderPos.x, renderPos.y,(float)(renderPos.x + width), (float)(renderPos.y + height));
		return toReturn;
	}
	public void toggleShadow()
	{
		renderer.toggleShadow();
	}

	public void spawnTextInput(TextBox textBox) {
		tb = textBox;
		//renderer.addRenderer("textbox");
		
	}

	public void destroyTextBox() {
		renderer.removeRenderer("textbox");
		tb = null;		
	}
}
