package com.dbryers.canvasgraph;

import java.util.ArrayList;

public class PathOps {
	
	public static float getMaxDistFromOrigin(ArrayList<Float> distancesFromOrigin)
	{
		float maxDist = 0;
		for(Float p : distancesFromOrigin)
		{
			if(p > maxDist)
				maxDist = p;
		}
		return maxDist;
	}
	public static float getMinDistFromOrigin(ArrayList<Float> distancesFromOrigin)
	{
		float minDist = Float.MAX_VALUE;
		for(Float p : distancesFromOrigin)
		{
			if(p < minDist)
				minDist = p;
		}
		return minDist;
	}
	public static float getAvgDistFromOrigin(ArrayList<Float> distancesFromOrigin)
	{
		float avg;
		float sum = 0;
		int count = 0;
		
		for(Float p : distancesFromOrigin)
		{
			sum += p;
			count ++;
		}
		avg = sum/count;
		return avg;
	}
	public static String approximateShape(ArrayList<Float> distancesFromOrigin)
	{
		if(isCircle(distancesFromOrigin))
			return "This shape is a circle";
		else
			return "Undetermined";
	}
	private static float variance(ArrayList<Float> distancesFromOrigin, float avg)
	{
		float sum = 0;
        for (int i = 0; i < distancesFromOrigin.size(); i++) {
            sum += (distancesFromOrigin.get(i) - avg) * (distancesFromOrigin.get(i) - avg);
        }
        return sum / (distancesFromOrigin.size() - 1);
	}
	private static boolean isCircle(ArrayList<Float> distancesFromOrigin)
	{
		float min, max, avg, threshold;
		min = getMinDistFromOrigin(distancesFromOrigin);
		avg = getAvgDistFromOrigin(distancesFromOrigin);
		max = getMaxDistFromOrigin(distancesFromOrigin);		
		threshold = (float)Math.sqrt((double)variance(distancesFromOrigin, max));
		
		if(Math.abs(min - avg) < threshold && Math.abs(max - avg) < threshold)
			return true;
		
		
		
		return false;
	}

}
