package com.dbryers.canvasgraph;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.content.Context;
import android.graphics.drawable.shapes.*;
import android.util.AttributeSet;

@SuppressLint("ClickableViewAccessibility")
@SuppressWarnings("unused")
class GraphCanvas extends View
{
	private Path drawPath;
	private Paint drawPaint, canvasPaint;
	private int paintColor = 0xFF000000;
	private Canvas drawCanvas;
	private Bitmap canvasBitmap;
	private int canvasWidth, canvasHeight;
	public ArrayList<Float> _distancesFromOrigin;
	
	public GraphCanvas(Context context, AttributeSet attrs){
		super(context, attrs);
		setupDrawing();
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh)
	{
		super.onSizeChanged(w,h,oldw,oldh);
		canvasWidth = w; canvasHeight = h;		
		canvasBitmap= Bitmap.createBitmap(canvasWidth,canvasHeight,Bitmap.Config.ARGB_8888);
		drawCanvas = new Canvas(canvasBitmap);
	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		canvas.drawColor(Color.TRANSPARENT);
		canvas.drawBitmap(canvasBitmap,0,0, canvasPaint);
		canvas.drawPath(drawPath, drawPaint);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		float touchX = event.getX();
		float touchY = event.getY();


		switch(event.getAction())
		{
			case MotionEvent.ACTION_DOWN:
				drawPath.moveTo(touchX, touchY);
				drawPath.lineTo(touchX + 50, touchY + 50);
				invalidate();
				break;
			case MotionEvent.ACTION_UP:
				drawPath.reset();
				clearGraph();
				invalidate();
			default:
				return false;
		}

		return true;
	}

	private void setupDrawing()
	{
		drawPaint = new Paint();
		drawPath = new Path();
		drawPaint.setColor(paintColor);
		drawPaint.setAntiAlias(true);
		drawPaint.setStrokeWidth(10);
		drawPaint.setStyle(Paint.Style.STROKE);
		drawPaint.setStrokeJoin(Paint.Join.ROUND);
		drawPaint.setStrokeCap(Paint.Cap.ROUND);
		canvasPaint = new Paint(Paint.DITHER_FLAG);
		_distancesFromOrigin = new ArrayList<Float>();
	}
	public void drawGraph(ArrayList<Point> toDraw)
	{
		Path graphPath = plotGraph(toDraw);
		drawCanvas.drawPath(graphPath, drawPaint);
		invalidate();
	}
	private Path plotGraph(ArrayList<Point> toGraph)
	{		
		int graphX = 0;
		Point centre;
		int size = toGraph.size();
		Path toReturn = new Path();
		
		centre = findCentre(toGraph);
		
		toReturn.moveTo(0, 0);
		for(int i = 0; i < size; i++)
		{
			Point p = toGraph.get(i);			
			float graphY = (float)Math.sqrt(Math.pow(p.x - centre.x, 2) + Math.pow(p.y - centre.y, 2));
			toReturn.lineTo((float)graphX, graphY);
			_distancesFromOrigin.add(graphY);
			graphX+=5;
		}		
		return toReturn;
	}
	public Point findCentre(ArrayList<Point> points)
	{
		int avgX = 0, avgY = 0;
		double sumX = 0, sumY = 0;
		Point centre;
		int size = points.size();
		for(int i = 0; i < size; i++)
		{			
			sumX += points.get(i).x;
			sumY += points.get(i).y;
		}
		avgX = (int)(sumX / size);
		avgY = (int)(sumY / size);	
		
		centre = new Point(avgX, avgY);
		
		return centre;
	}
	public void clearGraph()
	{
		drawCanvas.drawColor(Color.WHITE);
		invalidate();
	}
}
