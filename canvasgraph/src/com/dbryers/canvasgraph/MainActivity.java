 package com.dbryers.canvasgraph;

import java.util.ArrayList;

import android.app.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.content.Context;
import android.graphics.drawable.shapes.*;
import android.util.AttributeSet;

@SuppressWarnings("unused")
public class MainActivity extends Activity implements View.OnClickListener
{
	private DrawingCanvas drawView;
	private GraphCanvas graphView;
	private ImageButton drawBtn, eraseBtn;
	//private ArrayList<Drawable> drawnObjects;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
	{
        super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		drawView = (DrawingCanvas)findViewById(R.id.touch);
		graphView = (GraphCanvas)findViewById(R.id.graph);
		drawView.initGraph(graphView);
		drawBtn = (ImageButton)findViewById(R.id.draw_btn);
		drawBtn.setOnClickListener(this);
		eraseBtn = (ImageButton)findViewById(R.id.erase_btn);
		eraseBtn.setOnClickListener(this);
    }
	public void onClick(View view)
	{
		if(view.getId() == R.id.draw_btn)
		{
			drawView.setErase(false);
		}
		else if(view.getId() == R.id.erase_btn)
		{
			drawView.setErase(true);
		}
	}

 
		}	
