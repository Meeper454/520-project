package com.dbryers.canvasgraph;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.content.Context;
import android.graphics.drawable.shapes.*;
import android.util.AttributeSet;

@SuppressLint("ClickableViewAccessibility")
@SuppressWarnings("unused")
class DrawingCanvas extends View
{
	private GraphCanvas graph;
	private Path drawPath;
	private ArrayList<Point> pathPoints = new ArrayList<Point>();
	private Paint drawPaint, canvasPaint;
	private int paintColor = 0xFF000000;
	private Canvas drawCanvas;
	private Bitmap canvasBitmap;
	private boolean erase=false;
	private float mX, mY, lX, lY;

	public DrawingCanvas(Context context, AttributeSet attrs){
		super(context, attrs);
		setupDrawing();
	}
	
	public void initGraph(GraphCanvas toGraphOn)
	{
		graph = toGraphOn;
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh)
	{
		super.onSizeChanged(w,h,oldw,oldh);
		canvasBitmap= Bitmap.createBitmap(w,h,Bitmap.Config.ARGB_8888);
		drawCanvas = new Canvas(canvasBitmap);
	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		canvas.drawColor(Color.TRANSPARENT);
		canvas.drawBitmap(canvasBitmap,0,0, canvasPaint);
		canvas.drawPath(drawPath, drawPaint);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		float touchX = event.getX();
		float touchY = event.getY();


		switch(event.getAction())
		{
			case MotionEvent.ACTION_DOWN:
				drawPath.reset();
				drawPath.moveTo(touchX, touchY);
				mX=touchX; lX=touchX;
				mY=touchY; lY = touchY;
				graph.clearGraph();
				invalidate();
				break;
			case MotionEvent.ACTION_MOVE:
				float dx = Math.abs(touchX - mX);
				float dy = Math.abs(touchY - mY);
				if(dx>=4 || dy>=4)
				{
					if(erase)
					{
						drawPath.moveTo(lX, lY);
						drawPath.lineTo(touchX, touchY);
						drawCanvas.drawPath(drawPath, drawPaint);
						drawPath.reset();
						lX = touchX;
						lY = touchY;
					}
					else
					{
						drawPath.quadTo(mX, mY, (touchX + mX)/2,(mY +touchY)/2);
						//displayMessage("Point X = " + touchX + "; Point Y = " + touchY);
						Point toAdd = new Point((int)mX, (int)mY);
						pathPoints.add(toAdd);
						toAdd = new Point((int)(touchX + mX)/2, (int)(mY +touchY)/2);
						pathPoints.add(toAdd);
						mX = touchX;
						mY = touchY;
					}
					invalidate();
				}
				break;
			case MotionEvent.ACTION_UP:
				if(!erase)
				{
					drawCanvas.drawPath(drawPath, drawPaint);
					Point c = graph.findCentre(pathPoints);				
					graph.drawGraph(pathPoints);
					String message = PathOps.approximateShape(graph._distancesFromOrigin);
					graph._distancesFromOrigin.clear();
					pathPoints.clear();
					drawPath.reset();
					drawPath.moveTo(c.x, c.y);					
					drawPath.addCircle(c.x, c.y, (float) 1, Path.Direction.CW);					
					displayMessage(message);
				}
				
				drawCanvas.drawPath(drawPath, drawPaint);
				drawPath.reset();				
				invalidate();
				break;
			default:
				return false;
		}

		return true;
	}
	private void displayMessage(String message)
	{
		AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this.getContext());
		dlgAlert.setMessage(message);
		dlgAlert.setTitle("App Title");
		dlgAlert.setPositiveButton("OK", null);
		dlgAlert.setCancelable(true);
		dlgAlert.create().show();
	}
	private void setupDrawing()
	{
		drawPaint = new Paint();
		drawPath = new Path();
		drawPaint.setColor(paintColor);
		drawPaint.setAntiAlias(true);
		drawPaint.setStrokeWidth(10);
		drawPaint.setStyle(Paint.Style.STROKE);
		drawPaint.setStrokeJoin(Paint.Join.ROUND);
		drawPaint.setStrokeCap(Paint.Cap.ROUND);
		canvasPaint = new Paint(Paint.DITHER_FLAG);				
		
		//graph.clearGraph();
	}
	public void setErase(boolean isErase)
	{
		erase=isErase;
		if(erase)
		{
			drawPaint.setColor(Color.WHITE);
			drawPaint.setStrokeWidth(30);
			drawPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));	
		}
		else
		{
			drawPaint.setXfermode(null);
			drawPaint.setColor(Color.BLACK);
			drawPaint.setStrokeWidth(10);
		}
	}
}
